public with sharing class KNS_Integration_Utils {

    public static String dFilter;
    public static String knsBillStatusFilter;
    static {
        Date yesterday = System.today().addDays(-1);
        Date twoDaysAgo = System.today().addDays(-2);
        dFilter = '&$filter='+
                            'year(LastUpdatedDate)+eq+' + yesterday.year() + 
                            '+and+' + 'month(LastUpdatedDate)+eq+' + yesterday.month()  + 
                            '+and+' + 'day(LastUpdatedDate)+eq+' + yesterday.day() + 
                        '+or+' + 
                            'year(LastUpdatedDate)+eq+' + twoDaysAgo.year() + 
                            '+and+' + 'month(LastUpdatedDate)+eq+' + twoDaysAgo.month() +
                            '+and+' + 'day(LastUpdatedDate)+eq+' + twoDaysAgo.day();
        //dFilter = '&$filter=year(LastUpdatedDate)+eq+2019+and+month(LastUpdatedDate)+eq+10+and+day(LastUpdatedDate)+eq+28';
        //dfilter = '&$filter=KnessetNum+eq+21';

        knsBillStatusFilter = 'StatusID+eq+118';
    }

    public static IsraelLawsJson retrievedIsraelLawsJson;
    public static Map<String, Law__c> israelLawsToUpsert = new Map<String, Law__c>();
    public static List<Law__c> israelLawsWithoutIds = new List<Law__c>();

    public static BillsJson retrievedBillsJson;
    public static Map<String, Law__c> billsToUpsert = new Map<String, Law__c>();
    public static List<Law__c> billsWithoutIds = new List<Law__c>();

    public static LawsJson retrievedLawsJson;
    public static Map<String, Law__c> lawsToUpsert = new Map<String, Law__c>();
    public static List<Law__c> lawsWithoutIds = new List<Law__c>();

    public static LawBindingsJson retrievedLawBindingsJson;
    public static Map<String, LawBinding__c> lawBindingsToUpsert = new Map<String, LawBinding__c>();
    public static Map<String, Law__c> lawBindingsIdsMap = new Map<String, Law__c>();
    public static List<LawBinding__c> lawBindingsWithoutIds = new List<LawBinding__c>();
    //map between string errors and list of law bindings external ids
    public static Map<String, Map<String, List<String>>> lawBindingsErrorsMap = new Map<String, Map<String, List<String>>> {
                                                                                            'LawID' => new Map<String, List<String>>{
                                                                                                                                        'NullOrEmpty' => new List<String>(),
                                                                                                                                        'NoSFRecord' => new List<String>()
                                                                                                                                    },
                                                                                            'ParentLawID' => new Map<String, List<String>>{
                                                                                                                                        'NullOrEmpty' => new List<String>(),
                                                                                                                                        'NoSFRecord' => new List<String>()
                                                                                            },
                                                                                            'IsraelLawID' => new Map<String, List<String>>{
                                                                                                                                        //'NullOrEmpty' => new List<String>(),
                                                                                                                                        'NoSFRecord' => new List<String>()
                                                                                            }
                                                                                        };

    public KNS_Integration_Utils(){}

    public class IsraelLawsJson {
        public IsraelLawJson[] value;
        public String nextLink;
    }
    public class IsraelLawJson {
        public Integer IsraelLawID;	//2000002
	    public String Name;	//חוק התקשורת (בזק ושידורים), התשמ"ב-1982
	    public Integer LawValidityID;	//6079
	    public String LawValidityDesc;	//תקף
	    //public String LastUpdatedDate;	//2015-11-01T16:50:29.227
    }

    public class BillsJson{
        public BillJson[] value;
        public String nextLink; 
    }
    public class BillJson {
	    public Integer BillID;	
	    public Integer KnessetNum;	
	    public String Name;
	    public Integer SubTypeID;
        public String SubTypeDesc;
        public Integer StatusID;
    }

    public class LawsJson{
        public LawJson[] value;
        public String nextLink; 
    }
    public class LawJson {
	    public Integer LawID;	//2001673
	    public String TypeDesc;	//תיקון טעות
	    public String SubTypeDesc;	//תיקון לפי ס' 10א לפשס"מ – אחרי פרסום החוק
	    public String Name;	//תיקון טעות בחוק יישום ההסכם בדבר רצועת עזה ואזור יריחו (סמכויות שיפוט והוראות אחרות) (תיקוני חקיקה), התשנ"ה-1994
	    //public String PublicationDate;
    }

    public class LawBindingsJson{
        public LawBindingJson[] value;
        public String nextLink; 
    }
    public class LawBindingJson {
	    public Integer LawBindingID;	//54407
	    public Integer LawID;	//568879
	    public Integer IsraelLawID;	//2002349
	    public Integer ParentLawID;	//2002349
	    public String BindingTypeDesc;	//החוק המקורי
    }





    public static void getKnessetIsraelLaws(){
        Id rtId = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Parent_Law').getRecordTypeId();

        String KNSObjectName = 'KNS_IsraelLaw';
        String dateFilter = '';
        dateFilter = dFilter;
        String orderBy = '&$orderby=IsraelLawID';
        String nxtLink = '';

        Boolean hasId;
        do {
            calloutGetData(KNSObjectName, nxtLink, dateFilter, orderBy);
            System.debug('>>>>>RetrievedJsonDeserialized: ' + retrievedIsraelLawsJson);
            if( retrievedIsraelLawsJson == null) {
                System.debug('ERROR!');
                return; 
            }
            if( retrievedIsraelLawsJson.value != null) {
                for(IsraelLawJson israelLaw : retrievedIsraelLawsJson.value){
                    hasId = true;
                    Law__c il = new Law__c(Law_Name__c = israelLaw.Name, LawValidityDesc__c = israelLaw.LawValidityDesc, RecordTypeId = rtId);
                    if( israelLaw.IsraelLawID != null){
                        System.debug('israelLaw.IsraelLawID: ' +israelLaw.IsraelLawID);
                        String id = convertKNSIdToString(israelLaw.IsraelLawID);
                        System.debug('>>>>>id ' +id);
                        if( !String.isEmpty(id) ){
                            il.LawID__c = id;
                            //I dont know why to use this convert
                            israelLawsToUpsert.put(il.LawID__c, il);
                        } else {
                            hasId = false;
                        }
                    } else {
                        hasId = false;
                    }
                    if(hasId == false) {
                         il.LawID__c = null;
                        israelLawsWithoutIds.add(il);
                    }
                }
            }

            if ( retrievedIsraelLawsJson.nextLink != null && !String.isEmpty(retrievedIsraelLawsJson.nextLink) ) {
                nxtLink = retrievedIsraelLawsJson.nextLink;
            } else {
                break;
            }
        } while (nxtLink != null && !String.isEmpty(nxtLink) );

        System.debug('>>>>> Israel lawsToUpsert: ' + israelLawsToUpsert);
        System.debug('>>>>> ISREAL LAWS Withot ids: ' + israelLawsWithoutIds);
    }

    public static void upsertKnessetIsraelLaws(){
        if(israelLawsToUpsert.size() > 0) {
            upsert israelLawsToUpsert.values() LawID__c;
        }
    }

    public static void getKnessetBills(){
        Id rtId = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Law').getRecordTypeId();

        String KNSObjectName = 'KNS_Bill';
        String dateFilter = '';
        dateFilter = dFilter;

        String filter = dateFilter + '+and+' + knsBillStatusFilter;
        System.debug('>>>>>bills filter' + filter);

        String orderBy = '&$orderby=BillID';
        String nxtLink = '';

        Boolean hasId;
        do {
            calloutGetData(KNSObjectName, nxtLink, filter, orderBy);
            System.debug('>>>>>RetrievedJsonDeserialized: ' + retrievedBillsJson);
            if( retrievedBillsJson == null) {
                System.debug('ERROR!');
                return; 
            }
            if(retrievedBillsJson.value != null){
                for(BillJson bill : retrievedBillsJson.value){
                    hasId = true;
                    Law__c b = new Law__c(SubTypeDesc__c = bill.SubTypeDesc, RecordTypeId = rtId);
                    String name = bill.Name;
                    if(name != null && !String.isEmpty(name) ) {
                        if(name.length() > 80){
                            name = name.substring(0, 80);
                        }
                    }
                    b.Law_Name__c = name;
                    if( bill.BillID != null){
                        String id = convertKNSIdToString(bill.BillID);
                        if( !String.isEmpty(id) ){
                            b.LawID__c = id;
                            billsToUpsert.put(id, b);
                        } else {
                            hasId = false;
                        }
                    } else {
                        hasId = false;
                    }
                    if(hasId == false){
                        b.LawID__c = null;
                        billsWithoutIds.add(b);
                    }
                }   
            }

            if ( retrievedBillsJson.nextLink != null && !String.isEmpty(retrievedBillsJson.nextLink) ) {
                nxtLink = retrievedBillsJson.nextLink;
            } else {
                break;
            }
        } while (nxtLink != null && !String.isEmpty(nxtLink) );

        System.debug('>>>>> billsToUpsert: ' + billsToUpsert);
        System.debug('>>>>>Bills withot ids: ' + billsWithoutIds);
    }

    public static void upsertKnessetBills(){
        if(billsToUpsert.size() > 0) {
            System.debug('>>>>>upsert bills');
            upsert billsToUpsert.values() LawID__c;
        }
    }

    public static void getKnessetLaws(){
        Id rtId = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Law').getRecordTypeId();

        String KNSObjectName = 'KNS_Law';
        String dateFilter = '';
        dateFilter = dFilter;
        String orderBy = '&$orderby=LawID';
        String nxtLink = '';

        Boolean hasId;
        do {
            calloutGetData(KNSObjectName, nxtLink, dateFilter, orderBy);
            System.debug('>>>>>RetrievedJsonDeserialized: ' + retrievedLawsJson);
            if( retrievedLawsJson == null) {
                System.debug('ERROR!');
                return; 
            }
            if(retrievedLawsJson.value != null){
                for(LawJson law : retrievedLawsJson.value){
                    hasId = true;
                    Law__c l = new Law__c(TypeDesc__c = law.TypeDesc, SubTypeDesc__c = law.SubTypeDesc, RecordTypeId = rtId);
                    String name = law.Name;
                    if(name != null && !String.isEmpty(name) ) {
                        if(name.length() > 80){
                            name = name.substring(0, 80);
                        }
                    }
                    l.Law_Name__c = name;
                    if( law.LawID != null){
                        String id = convertKNSIdToString(law.LawID);
                        if( !String.isEmpty(id) ){
                            l.LawID__c = id;
                            lawsToUpsert.put(l.LawID__c, l);
                        } else {
                            hasId = false;
                        }
                    } else {
                        hasId = false;
                    }
                    if(hasId == false){
                        l.LawID__c = null;
                        lawsWithoutIds.add(l);
                    }
                }   
            }

            if ( retrievedLawsJson.nextLink != null && !String.isEmpty(retrievedLawsJson.nextLink) ) {
                nxtLink = retrievedLawsJson.nextLink;
            } else {
                break;
            }
        } while (nxtLink != null && !String.isEmpty(nxtLink) );

        System.debug('>>>>> lawsToUpsert: ' + lawsToUpsert);
        System.debug('>>>>>Laws withot ids: ' + lawsWithoutIds);
    }

    public static void upsertKnessetLaws(){
        if(lawsToUpsert.size() > 0 ){
            upsert lawsToUpsert.values() LawID__c;
        }
    }

    public static void getKnessetLawBindings(){
        String KNSObjectName = 'KNS_LawBinding';       
        String dateFilter = '';
        dateFilter = dFilter;
        String orderBy = '&$orderby=LawBindingID';
        String nxtLink = '';

        //List<Law__c> allExistingLaws = [SELECT Id, LawID__c FROM Law__c];
        for(Law__c l : [SELECT Id, LawID__c FROM Law__c]){
            //external id to law
            lawBindingsIdsMap.put(l.LawID__c, l);
        }

        boolean lawIdFlag;
        boolean parentIdFlag;
        boolean isValidIsraelLaw;

        Boolean hasLBId;
        Boolean hasLawId;
        Boolean hasParentId;


        do { 
            calloutGetData(KNSObjectName, nxtLink, dateFilter, orderBy);
            System.debug('>>>>>RetrievedJsonDeserialized: ' + retrievedLawBindingsJson);
            if( retrievedLawBindingsJson == null) {
                System.debug('ERROR!');
                return; 
            }

            if(retrievedLawBindingsJson.value != null){
                for(LawBindingJson lb : retrievedLawBindingsJson.value){
                    hasLBId = true;
                    hasParentId = true;
                    hasLawId = true;

                    lawIdFlag = false;
                    parentIdFlag = false;
                    isValidIsraelLaw = false;

                    LawBinding__c salesforceLb = new LawBinding__c(BindingTypeDesc__c = lb.BindingTypeDesc);
                    if(lb.LawBindingID != null){
                        String lbId = convertKNSIdToString(lb.LawBindingID);
                        if(!String.isEmpty(lbId)){
                            salesforceLb.LawBindingID__c = lbId;

                            if(lb.LawID != null){
                                String lawId = convertKNSIdToString(lb.LawID);
                                if( !String.isEmpty(lawId) ){
                                    if(lawBindingsIdsMap.containsKey(lawId) ){
                                        salesforceLb.LawID__c = lawBindingsIdsMap.get(lawId).Id;
                                        lawIdFlag = true;
                                    } else{
                                        System.debug('>>>>>EXternal law Id doesnt have record in SF. EXTERNAL ID:' + lawId);
                                        lawBindingsErrorsMap.get('LawID').get('NoSFRecord').add(lbId);
                                    }
                                } else {
                                    hasLawId = false;
                                }
                            } else {
                                hasLawId = false;
                            }

                            if(hasLawId == false){
                                System.debug('>>>>>Law binding doesnt have law id: external law binding id:' + lbId);
                                lawBindingsErrorsMap.get('LawID').get('NullOrEmpty').add(lbId);
                            }

                            if(lb.ParentLawID != null){
                                String parentId = convertKNSIdToString(lb.ParentLawID);
                                if( !String.isEmpty(parentId) ){
                                    if(lawBindingsIdsMap.containsKey(parentId) ){
                                        salesforceLb.ParentLawID__c = lawBindingsIdsMap.get(parentId).Id;
                                        parentIdFlag = true;
                                    } else{
                                        System.debug('>>>>>EXternal parent Id doesnt have record in SF. EXTERNAL ID:' + parentId);
                                        lawBindingsErrorsMap.get('ParentLawID').get('NoSFRecord').add(String.valueOf(lb.LawBindingID));
                                    }
                                } else {
                                    hasParentId = false;
                                }
                            } else {
                                hasParentId = false;
                            }

                            if(hasParentId == false){
                                System.debug('>>>>>Law binding doesnt have parent id: external law binding id:' + lb.LawBindingID);
                                lawBindingsErrorsMap.get('ParentLawID').get('NullOrEmpty').add(lbId);
                            }

                            if(lb.IsraelLawID != null){
                                String ilId = convertKNSIdToString(lb.IsraelLawID);
                                if( !String.isEmpty(ilId) ){
                                    if(lawBindingsIdsMap.containsKey(ilId) ){
                                        salesforceLb.IsraelLawID__c = lawBindingsIdsMap.get(ilId).Id;
                                        isValidIsraelLaw = true;
                                    } else{
                                        System.debug('>>>>>EXternal israel law Id doesnt have record in SF. EXTERNAL ID:' + convertKNSIdToString(lb.IsraelLawID));
                                        //flag is false
                                        lawBindingsErrorsMap.get('IsraelLawID').get('NoSFRecord').add(String.valueOf(lb.LawBindingID));
                                    }
                                } else {
                                    //empty//is posiible
                                    System.debug('>>>>>Law binding doesnt have israel law id: external law binding id:' + lb.LawBindingID);
                                    isValidIsraelLaw = true;
                                }
                            } else {
                                //null//is posiible
                                System.debug('>>>>>Law binding doesnt have israel law id: external law binding id:' + lb.LawBindingID);
                                isValidIsraelLaw = true;
                            }

                            if(lawIdFlag && parentIdFlag && isValidIsraelLaw){
                                lawBindingsToUpsert.put(salesforceLb.LawBindingID__c, salesforceLb);
                            } else {

                            }
                            
                        } else {
                                System.debug('>>>>>ID is empty;');
                                hasLBId = false;
                        }
                    } else {
                        System.debug('>>>>>ID is null;');
                        hasLBId = false;
                    }

                    if(hasLBId == false){
                        salesforceLb.LawBindingID__c = lb.LawBindingID == null ? null : '';
                        salesforceLb.LawID__c = '' + lb.LawID;
                        salesforceLb.ParentLawID__c = '' + lb.ParentLawID;
                        salesforceLb.IsraelLawID__c = '' + lb.IsraelLawID;
                        lawBindingsWithoutIds.add(salesforceLb);
                    }
                }
            }

            if ( retrievedLawBindingsJson.nextLink != null && !String.isEmpty(retrievedLawBindingsJson.nextLink) ) {
                nxtLink = retrievedLawBindingsJson.nextLink;
            } else {
                break;
            }
        } while (nxtLink != null && !String.isEmpty(nxtLink) );

        System.debug('>>>>>bindings TO UPsert: ' + lawBindingsToUpsert);
        System.debug('>>>>>bindings errors: ' + lawBindingsErrorsMap);
        System.debug('>>>>>bindings without ids: ' + lawBindingsWithoutIds);
    }

    public static void upsertKnessetLawBindings(){
        //check size>0 and upsert
        if(lawBindingsToUpsert.size() > 0) {
            upsert lawBindingsToUpsert.values() LawBindingID__c;
        }
    }

    public static void calloutGetData(String KNSObjectName, String nxtLink, String filter, String orderBy){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        String endPoint = 'http://knesset.gov.il/Odata/ParliamentInfo.svc/';
        if( nxtLink != null && nxtLink != '' && !String.isEmpty(nxtLink) ){
            endPoint += nxtLink + '&$format=json';
        } else {
            endpoint+= KNSObjectName + '?$format=json' + orderBy + filter;
        }
        req.setEndpoint(endPoint); 
        HttpResponse res = h.send(req);

        String body = res.getBody();
        System.debug('>>>>>Response Body: ' + body);
        body = body.replace('odata.nextLink', 'nextLink');

        if(KNSObjectName == 'KNS_IsraelLaw'){
            retrievedIsraelLawsJson = (IsraelLawsJson)JSON.deserialize(body, IsraelLawsJson.class);
        } else if(KNSObjectName == 'KNS_Law'){
            retrievedLawsJson = (LawsJson)JSON.deserialize(body, LawsJson.class);
        } else if(KNSObjectName == 'KNS_LawBinding') {
            retrievedLawBindingsJson = (LawBindingsJson)JSON.deserialize(body, LawBindingsJson.class);
        } else if(KNSObjectName == 'KNS_IsraelLawClassificiation'){

        } else if(KNSObjectName == 'KNS_Bill'){
            retrievedBillsJson = (BillsJson)JSON.deserialize(body, BillsJson.class);
        } else {

        }
    }

    public static String convertKNSIdToString(Decimal KNSId){
        return String.valueOf(KNSId).removeEnd('.0');
    }
}
