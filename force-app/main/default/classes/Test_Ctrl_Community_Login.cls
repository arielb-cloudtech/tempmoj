@isTest
public with sharing class Test_Ctrl_Community_Login {
    
    @isTest
    public static void testGetUser (){
        //querying an id of the current user context
        List<User> users = [Select Id From User Where Id =: Userinfo.getUserId() LIMIT 1];
        //tested method call
        List<User> returnedList =Ctrl_Community_Login.getUser(users[0].Id);
    }

    @isTest
    public static void testLoginUser(){
        //querying current user record details
        List<User> users = [select id,username from User where id = :Userinfo.getUserId() LIMIT 1];
        //tested method call, null in the url field to cover the if statement doesnt work
        //String uId = Ctrl_Community_Login.LoginUser(users[0].username, 'teagonet01', null);
        //tested method call, wrong password to reach ref == null
        String uId2 = Ctrl_Community_Login.LoginUser(users[0].username, 'teagonet01gfgg', null);
    }

    @isTest
    public static void testLostPassword(){
        //querying current user record details
        List<User> users = [select id,username from User where id = :Userinfo.getUserId() LIMIT 1];
        //tested method call, correct user name
        Boolean f = Ctrl_Community_Login.LostPassword(users[0].Username);
        //tested method call, incorrect username to cover rec is empty
        Boolean f2 = Ctrl_Community_Login.LostPassword('dnekgibnerigbbidebisbi');
    }

    @TestSetup
    static void makeData(){

    }
}