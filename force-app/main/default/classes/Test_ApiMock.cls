@isTest
global class Test_ApiMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        // System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
		System.debug('+++req.getEndpoint(): ' + req.getEndpoint());
		HttpResponse res = new HttpResponse();
		if(req.getEndpoint().containsIgnoreCase('query')){
			System.debug('+++im inside first callout: ' );
			res.setHeader('Content-Type', 'application/json');
			res.setBody('{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"GlobalValueSet","records":[{"attributes":{"type":"GlobalValueSet","url":"/services/data/v42.0/tooling/sobjects/GlobalValueSet/0Nt1X0000004E9eSAE"},"Id":"0Nt1X0000004E9eSAE"}]}');
			res.setStatusCode(200);
		}else if(req.getEndpoint().containsIgnoreCase('PATCH')){
			res.setHeader('Content-Type', 'application/json');
			res.setBody('{"example":"test"}');
			res.setStatusCode(200);
		}
        return res;
    }
}