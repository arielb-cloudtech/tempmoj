@isTest
private class Tests 
{
	
	@isTest 
	static void test_method_one() 
	{
		Account acc = new Account(Name = 'test');
		insert acc;

		Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');
		ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;    

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.ShareType = 'I';
        contentlink.LinkedEntityId = acc.Id; 
        contentlink.ContentDocumentId = testcontent.ContentDocumentId;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;

        Law_Item__c li = new Law_Item__c();
        li.Name = 'test';
        insert li;

        Comment__c cmt = new Comment__c();
        cmt.Comment_Type__c = 'General Public Comment';
        insert cmt;

        Comment_Reply__c cmtr = new Comment_Reply__c();
        cmtr.Reply_to_Comment__c = cmt.Id;
        insert cmtr;
	}
	
	
	
}