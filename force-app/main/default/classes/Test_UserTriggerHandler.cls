@isTest
public with sharing class Test_UserTriggerHandler {

	@TestSetup static void makeData(){
		Account acc = new Account(Name ='community account');
		insert acc;
		Contact con = new Contact(Email = 'aadsf@adsf.afd', FirstName = 'community', LastName ='contact', AccountId = acc.Id, Role__c = 'Customer Community MOJ');
		insert con;
	}

	@isTest static void test_method_one(){
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community MOJ' LIMIT 1];
		Contact con = [SELECT Id FROM Contact WHERE Name = 'community Contact' LIMIT 1];

		UserRole ur = [SELECT Id FROM UserRole LIMIT 1];
		User u = new User(Alias = 'test123', Email = 'test123@noemail.com', Emailencodingkey = 'UTF-8',
							Lastname = 'Testing', Languagelocalekey = 'en_US', Localesidkey = 'en_US',
							ProfileId = p.Id, Country = 'United States', IsActive = true, ContactId = con.Id,
							Timezonesidkey = 'America/Los_Angeles', Username = 'tester@noemail.com'/* ,UserRoleId = ur.Id */);
		insert u;
	}
}
