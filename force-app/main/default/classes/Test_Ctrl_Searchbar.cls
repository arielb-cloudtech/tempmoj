@isTest
public with sharing class Test_Ctrl_Searchbar {
    public Test_Ctrl_Searchbar() {
        }

    @TestSetup
    static void makeData(){
        //data creation object
        ClsObjectCreator cls = new ClsObjectCreator();
        //accounts
        Account acc = cls.createAccount('king tomer');
        //system.debug(acc);
        //Law Items
        Law_Item__c li = cls.createLawItem('Draft', 'n', acc.Id);
        //system.debug(li);
        //comments
        cls.createComment('Draft');
        //system.debug(c1);
        Comment__c c = [Select Related_to_Law_Item__c From Comment__c Limit 1];
        c.Related_to_Law_Item__c = li.Id;
        //system.debug(c.Related_to_Law_Item__c);
        update c;
    }

    @isTest
    public static void test1RetrevieDataSearch(){
        Ctrl_Searchbar.RetrevieDataSearch('all');
    }

    @isTest
    public static void test2RetrevieDataSearch(){
        Ctrl_Searchbar.RetrevieDataSearch('n');
    }

    @isTest
    public static void testConstructor(){
        Ctrl_Searchbar csb = new Ctrl_Searchbar();
    }
}