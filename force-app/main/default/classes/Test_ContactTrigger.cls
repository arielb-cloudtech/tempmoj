@isTest
private class Test_ContactTrigger {	
	@isTest static void Test_InsertUpdateContact() {
		User thisUser =  [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() limit 1];
		//id thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() limit 1].id;    
        ClsObjectCreator cls = new ClsObjectCreator();
		List<RecordType> accRecTypes = [select Id,Name from RecordType where IsPersonType = false and  SobjectType='Account' And RecordType.Name = 'MOJ Contact'];
		List<Account> accounts = new List<Account>();
		accounts.add(cls.returnAccount('ToInsert',accRecTypes[0].Id));
		accounts.add(cls.returnAccount('משרד המשפטים',accRecTypes[0].Id));
		System.runAs (thisUser) {
			insert accounts;
		}
		cls.createPermissionSet('F_Secure_Cloud_Protection_User');
		//Profile prf = cls.getProfileByName('Referent');
		Contact con1 = cls.createContactWithAccount(accounts[1].id);
		List <Contact> thisContact = new List <Contact>();
		thisContact.add(con1);


		Test.startTest();
			System.enqueueJob(new contactToUpdateQueueable(thisContact));
			system.debug('^^^ Contact con1: ' + con1);
			String randomstr = string.valueof( (Math.floor(Math.random() * 999) + 1).intValue() ) ;
			Contact con = new Contact ( ID_Card__c = '111111118' ,  FirstName= 'Testing123'+randomstr ,LastName='Testing123'+randomstr,Email = 'Test'+randomstr+'@gmail.com' , Role__c = 'Referent'  ,  Active_User__c = true, AccountId= accounts[1].id);
			System.runAs (thisUser) {
				insert con;
			}
			//insert con;
			con1.User__c =thisUser.id;
			con1.Active_User__c = false;
			System.runAs (thisUser) {
				update con1;				
			}
		Test.stopTest();
	}
}