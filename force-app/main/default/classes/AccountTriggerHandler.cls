public with sharing class AccountTriggerHandler {
	private Boolean isChanged = false;
	public AccountTriggerHandler() {}
	
	private class GlobalValueSetWrapper {
		public List<Records> records; 
	}
	private class Records {
		public String Id;
	}

	public void classifyPersonAccount(Map<Id,Account> newAccountMap){
		Map<String, Schema.RecordTypeInfo> rt_mapByName;
		Schema.DescribeSObjectResult acc_dr = Schema.SObjectType.Account;
		rt_mapByName = acc_dr.getRecordTypeInfosByName();
		Id accRt = rt_mapByName.get('MOJ Contact').getRecordTypeId();		
		List<Account> valuesToInsert = new List<Account>();		
		for (Id accId : newAccountMap.keySet()) {
			if(newAccountMap.get(accId).RecordTypeId == accRt) {				
				//if insert and Moj Contact account
				valuesToInsert.add(newAccountMap.get(accId));				
			 }
		}
		if(!valuesToInsert.isEmpty()){
			insertToPicklist(valuesToInsert);
		}
	}

	private void insertToPicklist(List<Account> accListToInsert){
		//get the Units picklist Id in order to have it later for inserting the new metadata
		
		List<String> namesToAddToPicklist = filterExistingAccounts(accListToInsert, getPicklistCurrentValues());
		String fieldDef;
		//isChanged indicates if the values in the picklist has changed, if not - there is no need to do anything
		if (!namesToAddToPicklist.isEmpty() && isChanged) {
			// fieldDef = '{"Metadata":{"customValue":['+String.join(prepareValuesToInsert(namesToAddToPicklist),',' ).removeEnd(',')+'],"description":null,"masterLabel":"GemsofMyLife","sorted":false,"urls":null},"FullName":"Gems_of_My_Life"}';
			fieldDef = '{"Metadata":{"customValue":['+String.join(prepareValuesToInsert(namesToAddToPicklist),',' ).removeEnd(',')+'],"description":null,"masterLabel":"Units","sorted":false,"urls":null}}';
			System.debug('+++fieldDef: ' + fieldDef);
		}
		if(String.isNotBlank(fieldDef) ){
			AccountTriggerFutureHandler.updateFuture2(fieldDef);
		}
	}
	
	//gets the picklist values
	private List<String> getPicklistCurrentValues(){
		List<String> pickListValuesList = new List<String>();
		List<Schema.PicklistEntry> picklistUsingGlobal = User.fields.Unit__c.getDescribe().getPicklistValues();
		 for( Schema.PicklistEntry pickListVal : picklistUsingGlobal){
            pickListValuesList.add(pickListVal.getLabel());
        }    
        return pickListValuesList;
	}

	//this function purpose is to cycle through the current picklist global values and decide which to add and which to remove 
	private List<String> filterExistingAccounts(List<Account> accListToInsert,List<String> picklistCurrentValues){
		System.debug('+++accListToInsert: ' + accListToInsert);
		System.debug('+++picklistCurrentValues: ' + picklistCurrentValues);
		//names is the final filtered list which will be sent to the metadata api callout
		List<String> names = new List<String>();
		if(!accListToInsert.isEmpty()){
			//cycle through the accounts to add and check if they are not already in the picklist. if they are not the add them to the final filtered list
			for (Account acc : accListToInsert) {
				if(!picklistCurrentValues.contains(acc.Name)){
					names.add(acc.Name);
					isChanged = true;
				}
			}
		}
		System.debug('+++names: ' + names);
		picklistCurrentValues.addAll(names);
		
		
		return picklistCurrentValues;
	}
	
	private List<String> prepareValuesToInsert(List<String> newEntries){
		//this will be a list of json object that contains the metadata that needed 
		//example: {"color":null,"default":false,"description":null,"isActive":null,"label":"'+str+'","urls":null,"valueName":"'+str+'"}
		List<String> finalizedLst = new List<String>();
		for (String str : newEntries) {
		  	finalizedLst.add('{"color":null,"default":false,"description":null,"isActive":null,"label":"'+str+'","urls":null,"valueName":"'+str+'"}');
		}
		return finalizedLst;
	}
}