public without sharing class Utils {

    static {
        System.debug('>>>>>Initialization' + Pardot_Integeration__c.getAll().values());
    }
    public static final String BASE_URL = Pardot_Integeration__c.getAll().values()[0].BASE_URL__c;
    public static String APIUserKey = Pardot_Integeration__c.getAll().values()[0].API_Key__c;
    public static String APIKey = '';
    public static String UserEmail = Pardot_Integeration__c.getAll().values()[0].UserName__c;
    public static String UserPassword = Pardot_Integeration__c.getAll().values()[0].Password__c;
    //public Utils() {
        
    //}

    public static String CalloutBuilder(String Operation){
        HttpRequest req = new HttpRequest(); 
        Map<String,String> resultMap = new Map<String,String>();

        Http http = new Http();
        req = authenticationRequest(req);
        System.debug('req:'+req);
        System.debug('req body:'+req.getBody());
        HTTPResponse res = http.send(req);
        system.debug(res.getHeader('Location'));
        if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            res = new Http().send(req);
        }
        system.debug(res);
        system.debug(res.getbody());
        String loginKey;
        if(res.getStatusCode() == 200){
            String StringBody = String.valueOf(res.getbody());
            //if(Test.isRunningTest()){
            //    StringBody='<api_key>asdd1651315</api_key>';
            //}

            if(StringBody.containsIgnoreCase('api_key')){
                loginKey = StringBody.substringBetween('<api_key>', '</api_key>');
            }
            APIKey = loginKey;
            system.debug('^^^ loginKey '+ loginKey);
        }
        
        //switch on (Operation){
        //    When 'authentication' {
                return loginKey;
            //}
           /* When 'CreateList' {
                String listId = createList();
                return listId;
            }  
            When else {
                return null;
            } */
        //}

    }


    public static HttpRequest authenticationRequest(HttpRequest req){
        Map<String,String> params = new Map<String,String>();
        params.put('email', UserEmail);
        params.put('password', UserPassword);
        params.put('user_key', APIUserKey);

        String RequestBody = '';
        for(String s: params.keySet()){
            RequestBody += s + '=' +EncodingUtil.urlEncode(params.get(s),'UTF-8')+'&';
        }
        RequestBody =RequestBody.removeEnd('&');

        system.debug(RequestBody);
        String endPoint = BASE_URL+'api/login/version/4';/*/?'+ RequestBody+'/'*/
        //String RequestBody = 'email='+ EncodingUtil.urlEncode(UserEmail, 'UTF-8') + '&password='+ EncodingUtil.urlEncode(UserPassword, 'UTF-8')+  '&user_key='+ EncodingUtil.urlEncode(APIUserKey, 'UTF-8') ;
        //[ BLOCK TO BUILD URL]
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setBody(RequestBody);
        /* BLOCK TO SET HEADERS
        req.setHeader('SOAPAction','https://www.simplesms.co.il/webservice/SendMultiSms');*/

        req.setTimeout(120000); /// MAX time Out
        //req.setBody(xmlBuilder(Sender,CliTextMap));
        return req;
    }

    //@callout=true
    public static String createList(String thisAPIKey){
        
        Map<String,String> BodyParams = new Map<String,String>();
        Map<String,String> HeaderParams = new Map<String,String>();
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        String endPoint = BASE_URL+'api/list/version/4/do/create';/*/?'+ RequestBody+'/'*/
        req.setMethod('GET');
        req.setEndpoint(endPoint);
        
        HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(thisAPIKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
        
        BodyParams.put('name', 'SalesForce List '+String.valueOf(system.now() ));
        String RequestBody = '';
       
        for(String s: BodyParams.keySet()){
            RequestBody += s + '=' +EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8')+'&';
        }
        RequestBody = RequestBody.removeEnd('&');
        req.setBody(RequestBody);

        for(String s: HeaderParams.keySet()){
            req.setHeader(s,HeaderParams.get(s));
        }

        req.setTimeout(120000); /// MAX time Out
        HTTPResponse res = new HTTPResponse();
        res = http.send(req);
        //system.debug(res.getHeader('Location'));
        if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            res = new Http().send(req);
        }
        system.debug(res);
        system.debug(res.getbody());
        String responseBody = String.valueOf(res.getbody());
        if(responseBody.containsIgnoreCase('<errcode="1">')){
            ApiKey = CalloutBuilder('authentication');
            HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(ApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
            req.setHeader('Authorization',HeaderParams.get('Authorization'));
            res = new Http().send(req);
            responseBody = String.valueOf(res.getbody());
        }

        //if(Test.isRunningTest()){
        //    responseBody='<list><id>15151</id></list>';
        //}

        if(responseBody.containsIgnoreCase('<list>') && responseBody.containsIgnoreCase('<id>')){
            return (responseBody.substringBetween('<list>','</list>')).substringBetween('<id>','</id>');
        }

        return null;
    }

    public static String assignMembersToList(String apiKey, String ListId, List<String> prospects, Boolean isNew){
        
        if(prospects!=null && !prospects.isEmpty()){
            system.debug('^^^ (assignMembersToList) apiKey '+ apiKey);
            system.debug('^^^ (assignMembersToList) prospects '+ prospects);
            system.debug('^^^ (assignMembersToList) isNew '+ isNew);

            Map<String,String> BodyParams = new Map<String,String>();
            Map<String,String> HeaderParams = new Map<String,String>();
            HttpRequest req = new HttpRequest();
            Http http = new Http();

            String endPoint = BASE_URL+'api/listMembership/version/4/do/create';/*/?'+ RequestBody+'/'*/
            req.setMethod('POST');
            req.setEndpoint(endPoint);
            
            HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(apiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
            
            BodyParams.put('name', 'SalesForce List '+String.valueOf(system.now() ));
            BodyParams.put('list_id', ListId);
            BodyParams.put('prospect_id', prospects[0]);
            String RequestBody = '';
           
            for(String s: BodyParams.keySet()){
                RequestBody += s + '=' +EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8')+'&';
            }
            RequestBody = RequestBody.removeEnd('&');
            req.setBody(RequestBody);

            for(String s: HeaderParams.keySet()){
                req.setHeader(s,HeaderParams.get(s));
            }

            req.setTimeout(120000); /// MAX time Out
            HTTPResponse res = new HTTPResponse();
            res = http.send(req);
            //system.debug(res.getHeader('Location'));
            if(res.getStatusCode() == 302){
                req.setEndpoint(res.getHeader('Location'));
                res = new Http().send(req);
            }
            system.debug(res);
            system.debug(res.getbody());
            String responseBody = String.valueOf(res.getbody());
            if(responseBody.containsIgnoreCase('<errcode="1">')){
                String newApiKey = CalloutBuilder('authentication');
                HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(newApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
                req.setHeader('Authorization',HeaderParams.get('Authorization'));
                res = new Http().send(req);
                responseBody = String.valueOf(res.getbody());
            }

            if(responseBody.containsIgnoreCase('<list>') && responseBody.containsIgnoreCase('<id>')){
                return (responseBody.substringBetween('<list>','</list>')).substringBetween('<id>','</id>');
            }
        }

        return null;
    }

    // private static String RemoveFromList(String apiKey, String ListId, List<String> prospects, Boolean isNew){
        
              //      Map<String,String> BodyParams = new Map<String,String>();
              //      Map<String,String> HeaderParams = new Map<String,String>();
              //      HttpRequest req = new HttpRequest();
              //      Http http = new Http();

              //      String endPoint = BASE_URL+'api/list/version/4/do/create';/*/?'+ RequestBody+'/'*/
                    //req.setMethod('GET');
              //      req.setEndpoint(endPoint);
                    
              //      HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(apiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
                    
              //      BodyParams.put('name', 'SalesForce List '+String.valueOf(system.now() ));
              //      String RequestBody = '';
                   
              //      for(String s: BodyParams.keySet()){
              //          RequestBody += s + '=' +EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8')+'&';
              //      }
              //      RequestBody = RequestBody.removeEnd('&');
              //      req.setBody(RequestBody);

              //      for(String s: HeaderParams.keySet()){
              //          req.setHeader(s,HeaderParams.get(s));
              //      }

              //      req.setTimeout(120000); /// MAX time Out
              //      HTTPResponse res = new HTTPResponse();
              //      res = http.send(req);
              //      //system.debug(res.getHeader('Location'));
              //      if(res.getStatusCode() == 302){
              //          req.setEndpoint(res.getHeader('Location'));
              //          res = new Http().send(req);
              //      }
              //      system.debug(res);
              //      system.debug(res.getbody());
              //      String responseBody = String.valueOf(res.getbody());
              //      if(responseBody.containsIgnoreCase('<list>') && responseBody.containsIgnoreCase('<id>')){
              //        return (responseBody.substringBetween('<list>','</list>')).substringBetween('<id>','</id>');
              //      }
        // return null;
    // }

    // public static Map<String,String> createTextContent(Id lawItemId, String lang, Map<String,String> links, Map<String,String> mainlinks, Map<String,String> rialinks, Map<String,String> comblinks){
    //     Map<String,String> res = new Map<String,String>();
    //     //res = Util_Email_Templates.SendEmail(lawItemId, lang, links, mainlinks, rialinks, comblinks);
    //     res = Util_Email_Templates.SendEmail(lawItemId, lang, mainlinks, rialinks, comblinks);
    //     return res;
    // }

    // public static Map<String,String> createTextContentSummary(Id lawItemId, String lang, Map<String,String> links, Map<String,String> mainlinks, Map<String,String> rialinks, Map<String,String> comblinks){
    //     return new Map<String,String>();
    // }




    public static void sendEmailsToLists(String thisAPIKey, String thisList, Map<String,object> BodyParams ){/*, Set<String> links*/
        system.debug('sendEmailsToLists');

        APIKey = thisAPIKey;
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        system.debug('thisList '+thisList);
        system.debug('ApiKey ' + ApiKey);
        if(APIKey == null || String.isBlank(APIKey)){
            ApiKey = CalloutBuilder('authentication');
        }

        Map<String,String> HeaderParams = new Map<String,String>();
        //BodyParams.put('subject', subject);
        //BodyParams.put('email_template_id', 3872);
        //BodyParams.put('text_content', text_content);
        
        BodyParams.put('campaign_id', Integer.ValueOf(Pardot_Campaign__c.getAll().get('Email Distribution').PardotId__c));
        //BodyParams.put('format', 'json');
        if(thisList!=null){
            BodyParams.put('list_ids[]', Integer.ValueOf(thisList));
        }
        
        Integer otherList;
        if(BodyParams.containsKey('list_id[]CS')){
            otherList = integer.ValueOf(BodyParams.get('list_id[]CS'));
            BodyParams.remove('list_id[]CS');
        }
            
        //HeaderParams.put('list_ids[]', thisList);
        HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(ApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
        system.debug('Pardot '+'api_key='+EncodingUtil.urlEncode(ApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
         HeaderParams.put('Accept','*/*');
       
        HeaderParams.put('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Authorization',HeaderParams.get('Authorization'));
        //HeaderParams.put('Content-Type','application/json');
        //req.setHeader('list_ids[]','2518');
        String HtmlBody='';
        String RequestBody = '';
        Map<String, Object> content = new Map<String, Object>();
        for(String s: BodyParams.keySet()){
            if(!s.equalsIgnoreCase('html_content')/* && !s.equalsIgnoreCase('text_content')*/){
                RequestBody += s + '=' +EncodingUtil.urlEncode(String.valueOf(BodyParams.get(s)),'UTF-8')+'&';
            }else{
                //if(s.equalsIgnoreCase('html_content')){
                    //content.put('html_content', EncodingUtil.urlEncode(String.valueOf(BodyParams.get(s)),'UTF-8'));
                    content.put('html_content', String.valueOf(BodyParams.get(s).toString()));
                //}else{
                //    content.put('text_content',String.valueOf(BodyParams.get(s)));
                //}
            }
            //HeaderParams.put(s,EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8'));
            //req.setHeader(s,EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8'));
        }

        HtmlBody = 'html_content='+ EncodingUtil.urlEncode(String.valueOf(content.get('html_content')),'UTF-8');
        //content.put('html_content','Hello World %%unsubscribe%%');
        //HtmlBody = JSON.serialize(content);
        //system.debug(req.getHeader());
        //RequestBody =RequestBody.removeEnd('&');
        if(otherList!=null){
            RequestBody += 'list_ids[]'+'=' +EncodingUtil.urlEncode(String.valueOf(otherList),'UTF-8');
        }else{
            RequestBody =RequestBody.removeEnd('&');
        }
        system.debug('RequestBody '+ RequestBody);
        system.debug('HtmlBody '+ HtmlBody);
        //RequestBody = Json.serialize(BodyParams);

        String endPoint = BASE_URL+'api/email/version/4/do/send/?'+ RequestBody+'/';
        system.debug('endPoint.length '+ endPoint.length());

        //String RequestBody = 'email='+ EncodingUtil.urlEncode(UserEmail, 'UTF-8') + '&password='+ EncodingUtil.urlEncode(UserPassword, 'UTF-8')+  '&user_key='+ EncodingUtil.urlEncode(APIUserKey, 'UTF-8') ;
        //[ BLOCK TO BUILD URL]
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setBody(HtmlBody);
        system.debug('find me '+req.getBody());
        /* BLOCK TO SET HEADERS
        req.setHeader('SOAPAction','https://www.simplesms.co.il/webservice/SendMultiSms');*/

        //system.debug(req.getBody());

        req.setTimeout(120000); /// MAX time Out
        //req.setBody(xmlBuilder(Sender,CliTextMap));
        //return req;
        HTTPResponse res = http.send(req);
        system.debug(res.getHeader('Location'));
        if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            res = new Http().send(req);
        }
        system.debug(res);
        system.debug(res.getbody());
        String loginKey;
        if(res.getStatusCode() == 200){
            /// i dono inform on success?
        }else{
            //// inform user on error
        }
    }

    public static Map<String,Set<String>> ReturnProspectsByLawItems(Set<Id> lawItemIds,String language){
        system.debug('lawItemIds '+ lawItemIds);
        system.debug('language '+ language);
        return collectingProspects(lawItemIds, 'pbl', Language);
    }


    public static Map<String,Set<String>> ReturnLawItemsByProspects(Set<Id> lawItemIds,String language){
        return collectingProspects(lawItemIds, 'lbp', Language);
    }

    /// DO NOT OPEN IF THERE IS A BUG HERE THE WINDOW IS SAFER PLEASE JUMP FORM IT
    private static Map<String,Set<String>> collectingProspects(Set<Id> lawItemIds, String opperation, String Language){

        Set<Law_Item__c> DistributedLawItems = new Set<Law_Item__c>(); //
        Set<String> recordTypeNames = new Set<String>(); // record type names of law items relevants
        Map<String,Id> recordTypeNameToId = new Map<String,Id>(); // record type names of law items to IDs Map relevants
        Map<String,Set<String>> prospectsListByItemType = new Map<String,Set<String>>(); // prospects Lists By ItemType
        Map<String,Set<String>> prospectsListByClassification = new Map<String,Set<String>>(); // prospects Lists By Classifications
        Map<String,Set<String>> prospectsListByAccount = new Map<String,Set<String>>(); // prospects Lists By Classifications
        Map<String,Set<String>> prospectsListByLaw = new Map<String,Set<String>>(); // prospects Lists By Classifications
        Map<String,Set<String>> prospectsListByDraft = new Map<String,Set<String>>(); // prospects Lists By Classifications
        Map<String,Set<String>> prospectsListByRelatedLaws = new Map<String,Set<String>>(); // prospects Lists By Classifications
        Map<Id,Set<Id>> lawItemsByRecTypes = new Map<Id,Set<Id>>(); /// maps law items By record Types
        Map<Id,Set<String>>LawItemToClassificationIds = new Map<Id,Set<String>>(); /// maps law items Id to classification_Ids
        Map<String,Set<Id>>LawItemByClassifications = new Map<String,Set<Id>>(); /// maps law items Id BY related classification_Ids
        Map<String,Set<Id>>LawItemByDraftType = new Map<String,Set<Id>>(); /// maps law items Id BY related classification_Ids      
        Map<Id,Set<Id>>LawItemByAccount = new Map<Id,Set<Id>>(); /// maps law items Id to classification_Ids
        Map<Id,Set<Id>>LawItemByLaw = new Map<Id,Set<Id>>(); /// maps law items Id to classification_Ids
        Map<Id,Set<String>>LawItemByRelatedLaws = new Map<Id,Set<String>>(); /// maps law items Id to classification_Ids
        Map<Id,Set<String>>LawItemToRelatedLaws = new Map<Id,Set<String>>(); /// maps law items Id to classification_Ids
        Set<Id> LawItemsWithRIAAttachments = new Set<Id>();
        Set<String> prospectsAllAndImmediate = new Set<String>();
        Set<String> prospectListByRIA = new Set<String>();
        /*Related_to_Laws__r*/

        Map<String, Set<String>> ProspectsByLawItem = new Map<String, Set<String>>();
        Map<String, Set<String>> LawItemsPerProspects = new Map<String, Set<String>>();

        Map<String,Set<Id>> allRelevantProspects = new Map<String,Set<Id>>();

        ////// Collecting Mailing Subscription categories STRAT
        for(Law_Item__c li : [SELECT Id, Status__c, Law__c, Type__c, Publisher_Account__c, RIA_Attachment__c, Law_Item__c,(SELECT Name, Id, Law__c, Authorization_Item__c FROM Related_to_Laws__r) RecordTypeId, RecordType.Name, (SELECT Id, Classification__c, Law_Item__c, Classification__r.ClassificiationID__c, Classification__r.Name FROM Law_Item_Classifications__r)  FROM Law_Item__c where Status__c != null AND Status__c ='Distributed' AND RecordTypeId != null AND Id In: lawItemIds]){
            
            DistributedLawItems.add(li);

            /// by record type name
            recordTypeNames.add( String.valueOf(RecordType.Name) );

            if(!lawItemsByRecTypes.containsKey(li.RecordTypeId)){
                lawItemsByRecTypes.put(li.RecordTypeId, new Set<Id>());
                recordTypeNameToId.put(li.RecordType.Name,li.RecordTypeId);
            }
            lawItemsByRecTypes.get(li.RecordTypeId).add(li.Id);

            if(!LawItemByAccount.containsKey(li.Publisher_Account__c)){
                LawItemByAccount.put(li.Publisher_Account__c,new Set<Id>());
            }
            LawItemByAccount.get(li.Publisher_Account__c).add(li.Id);
            system.debug('LawItemByAccount '+LawItemByAccount);

            if(!LawItemByLaw.containsKey(li.Law__c)){
                LawItemByLaw.put(li.Law__c,new Set<Id>());
            }
            LawItemByLaw.get(li.Law__c).add(li.Id);

            for(Law_Item_Classification__c lic : li.Law_Item_Classifications__r){
                if(lic.Classification__c != null && lic.Classification__r.ClassificiationID__c!= null){
                    if(!LawItemToClassificationIds.containsKey(lic.Law_Item__c)){
                        LawItemToClassificationIds.put(lic.Law_Item__c,new Set<String>());
                    }
                    LawItemToClassificationIds.get(lic.Law_Item__c).add(lic.Classification__r.ClassificiationID__c);
                    
                    if(!LawItemByClassifications.containsKey(lic.Classification__r.ClassificiationID__c)){
                        LawItemByClassifications.put(lic.Classification__r.ClassificiationID__c,new Set<Id>());
                    }
                    LawItemByClassifications.get(lic.Classification__r.ClassificiationID__c).add(lic.Law_Item__c);
                }
            }
            system.debug('LawItemByClassifications '+LawItemByClassifications);

            for(Related_to_Law__c rtl : li.Related_to_Laws__r ){
                if(rtl.Law__c != null){
                    if(!LawItemByRelatedLaws.containsKey(li.Id)){
                        LawItemByRelatedLaws.put(li.Id,new Set<String>());
                    }
                    LawItemByRelatedLaws.get(li.Id).add(rtl.Law__c);

                    if(!LawItemToRelatedLaws.containsKey(rtl.Law__c)){
                        LawItemToRelatedLaws.put(rtl.Law__c,new Set<String>());
                    }
                    LawItemToRelatedLaws.get(rtl.Law__c).add(li.Id);
                }
            } 
            system.debug('LawItemByRelatedLaws '+LawItemByRelatedLaws);


            if( li.RIA_Attachment__c!=null && li.RIA_Attachment__c.equalsIgnoreCase('Attach RIA Report')){
                LawItemsWithRIAAttachments.add(li.Id);
            }

            if(li.RecordType.Name.containsIgnoreCase('Secondary Legislation Draft') ){
                system.debug('Secondary Legislation Draft collecting Law items');
                if(!LawItemByDraftType.containsKey(li.RecordType.Name)){
                    LawItemByDraftType.put(li.RecordType.Name,new Set<Id>());
                }
                LawItemByDraftType.get(li.RecordType.Name).add(li.Id);              
            }
        }
        ////// Collecting Mailing Subscription categories END

        ////// Collecting Prospects by Mailing Subscription categories STRAT

        ///Step 1 Collecting prospect Ids from mailing subscribers on the relevant subject (record type of law item) 
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Personal_Preference_Type__c, Item_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language /* AND Personal_Preference_Type__c='Item Type'*/ AND Item_Type__c In : recordTypeNameToId.KeySet()  ) ]){
            if(!prospectsListByItemType.containsKey(ms.Item_Type__c)){
                prospectsListByItemType.put(ms.Item_Type__c,new Set<String>());
            }
            prospectsListByItemType.get(ms.Item_Type__c).add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }
        system.debug('prospectsListByItemType '+ prospectsListByItemType);

        ///Step 2 Collecting prospect Ids from mailing subscribers on the relevant classifications (Classification__c) 
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Personal_Preference_Type__c, Classification__r.ClassificiationID__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language /*AND Personal_Preference_Type__c='Classification'*/ AND Classification__r.ClassificiationID__c In : LawItemByClassifications.KeySet() ) ]){
            if(!prospectsListByClassification.containsKey(ms.Classification__r.ClassificiationID__c)){
                prospectsListByClassification.put(ms.Classification__r.ClassificiationID__c,new Set<String>());
            }
            prospectsListByClassification.get(ms.Classification__r.ClassificiationID__c).add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }
        system.debug('prospectsListByClassification '+ prospectsListByClassification);


        ///Step 3 Collecting prospect Ids from mailing subscribers on the relevant related Laws (Related_Law__c.Law__c) 
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Law__c, Personal_Preference_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Law__c In : LawItemByRelatedLaws.KeySet() ) ]){
            if(!prospectsListByRelatedLaws.containsKey(ms.Law__c)){
                prospectsListByRelatedLaws.put(ms.Law__c,new Set<String>());
            }
            prospectsListByRelatedLaws.get(ms.Law__c).add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }
        system.debug('prospectsListByRelatedLaws '+ prospectsListByRelatedLaws);

        ///Step 4 Collecting prospect Ids from mailing subscribers on the relevant Account (Publishing Ministry) 
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Ministry__c, Personal_Preference_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND /*Personal_Preference_Type__c='Publishing Ministry' AND*/ Ministry__c In : LawItemByAccount.KeySet() ) ]){
            if(!prospectsListByAccount.containsKey(ms.Ministry__c)){
                prospectsListByAccount.put(ms.Ministry__c,new Set<String>());
            }
            prospectsListByAccount.get(ms.Ministry__c).add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }
        system.debug('prospectsListByAccount '+ prospectsListByAccount);

        ///Step 5 Collecting prospect Ids from mailing subscribers on the relevant Law (Amended Law Memorandum) 
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Law__c, Personal_Preference_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND /*Personal_Preference_Type__c='Amended Law Memorandum' AND*/ Law__c In : LawItemByLaw.KeySet() ) ]){
            if(!prospectsListByLaw.containsKey(ms.Law__c)){
                prospectsListByLaw.put(ms.Law__c,new Set<String>());
            }
            prospectsListByLaw.get(ms.Law__c).add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }
        system.debug('prospectsListByLaw '+ prospectsListByLaw);

        ///Step 6 Collecting prospect Ids from mailing subscribers on the relevant RIA required (Personal_Preference_Type__c=Items with RIA) 
        system.debug('Language '+ Language);
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Personal_Preference_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Items_With_RIA__c=true ) ]){
            prospectListByRIA.add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }

        ///Step 7 Collecting prospect Ids from mailing subscribers on the relevant Secondary Legislation Draft
        String DraftOfSecondaryLaw = 'Secondary Legislation Draft%';
        for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Personal_Preference_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Personal_Preference_Type__c Like :DraftOfSecondaryLaw ) ]){
            system.debug('Secondary Legislation Draft% on Mailing_Subscription__c');
            if(!prospectsListByDraft.containsKey(ms.Personal_Preference_Type__c)){
                prospectsListByDraft.put(ms.Personal_Preference_Type__c,new Set<String>());
            }
            prospectsListByDraft.get(ms.Personal_Preference_Type__c).add(ms.Pardot_Prospect_Id__c);
            allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        }
        system.debug('prospectsListByDraft '+prospectsListByDraft);

        /// deprecated - mailing all and immidiate are done via customset prospect lists
        // for(Mailing_Subscription__c ms : [Select Id, Pardot_Prospect_Id__c, Personal_Preference_Type__c from Mailing_Subscription__c where ( Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Registration_Type__c ='All Updates' AND Mailing_Frequency__c='Immediate' ) ]){
        //     //prospectsAllAndImmediate.add(ms.Pardot_Prospect_Id__c);
        //     //allRelevantProspects.put(ms.Pardot_Prospect_Id__c, new Set<ID>());
        // }
        //system.debug('prospectsAllAndImmediate '+prospectsAllAndImmediate);

        ////// Collecting Prospects by Mailing Subscription categories END

        /// mapping ProspectsByLawItem Start START
        Map<String,Set<String>> LawItems2ProsFiltered = new Map<String,Set<String>>();
        System.debug('>>>>>opperation: ' + opperation);
        System.debug('>>>>>DistributedLawItems: ' + DistributedLawItems);
        for(Law_Item__c li : DistributedLawItems){
            if(!LawItems2ProsFiltered.containsKey(li.Id)){
                LawItems2ProsFiltered.put(li.Id, new Set<String>());
            }

            Set<String> relatedProspecSet = new Set<String>();

            if(!ProspectsByLawItem.containsKey(li.Id)){
                ProspectsByLawItem.put(li.Id, new Set<String>());
            }

            // adding step 1 results 
            if( li.RecordType!=null && prospectsListByItemType.containsKey( li.RecordType.Name )){
                relatedProspecSet.addAll( prospectsListByItemType.get(li.RecordType.Name) );
            }

            // adding step 2 results 
            system.debug('LawItemToClassificationIds '+LawItemToClassificationIds);
            system.debug('prospectsListByClassification '+prospectsListByClassification);

            if( LawItemToClassificationIds.containsKey(li.Id) && !(LawItemToClassificationIds.get(Li.Id).IsEmpty()) ){
                for(String lic : LawItemToClassificationIds.get(Li.Id)){
                    if(prospectsListByClassification.containsKey(lic)){
                        system.debug('^^^ ' +lic);
                        relatedProspecSet.addAll( prospectsListByClassification.get(lic) );
                    }
                }
            }

            // adding step 3 results 
            if( li.Publisher_Account__c!=null && prospectsListByAccount.containsKey( li.Publisher_Account__c )){
                relatedProspecSet.addAll( prospectsListByAccount.get(li.Publisher_Account__c) );
            }

            // adding step 4 results 
            if( li.Law__c!=null && prospectsListByLaw.containsKey( li.Law__c )){
                relatedProspecSet.addAll( prospectsListByLaw.get(li.Law__c) );
            }

            // adding step 5 results 
            if( LawItemsWithRIAAttachments.contains( li.Id )){
                relatedProspecSet.addAll( prospectListByRIA );
            }

            // adding step 5 results
            system.debug('li.Type__c '+li.Type__c ); 
            system.debug('prospectsListByDraft '+prospectsListByDraft ); 
            if(li.RecordType.Name.containsIgnoreCase('Secondary Legislation Draft') && li.Type__c!=null && !prospectsListByDraft.isEmpty()){
                for(String key : prospectsListByDraft.KeySet()){
                    system.debug('at list somthing '+key );
                    if( prospectsListByDraft.get(key)!=null ){
                        relatedProspecSet.addAll( prospectsListByDraft.get(key) );
                    }
                }
            }

            //relatedProspecSet.addAll(prospectsAllAndImmediate);

            ProspectsByLawItem.get(li.Id).addAll(relatedProspecSet);

            ///// SCREENING FOR COMBINATIONS
            Set<String> RelatedLaws = new Set<String>(); 
            if(prospectsListByRelatedLaws.containsKey(li.Id)){
                RelatedLaws = prospectsListByRelatedLaws.get(li.Id);
            }
            // LawItems2ProsFiltered = new Map<String,Set<String>>();
            String hasRIA = li.RIA_Attachment__c == 'Attach RIA Report'? 'Items with RIA' : 'RIA Report Not Required'; /// work around to fit query

            //for(Law_Item__c li : DistributedLawItems){
            for(Contact con : [SELECT Id, Pardot_Prospect_Id__c, 
                                    (SELECT Id, Ministry__c, Include_Comments_Updates__c FROM Mailing_Subscriptions__r 
                                            WHERE   ( (Registration_Type__c = 'Items with RIA' AND Registration_Type__c =: hasRIA) OR Registration_Type__c != 'Items with RIA' ) AND
                                                    (Ministry__c =: li.Publisher_Account__c OR Ministry__c = null) AND
                                                    (Law_Item__c =: li.Law_Item__c OR Law_Item__c = null) AND
                                                    (Law__c =: li.Law__c OR (Law__c IN : RelatedLaws) OR Law__c = null) AND
                                                    (Classification__c = null OR Classification__r.ClassificiationID__c IN : LawItemToClassificationIds.get(li.Id) )
                                                        ORDER BY Include_Comments_Updates__c DESC Limit 1)                                                                                     
                                                            FROM Contact 
                                                                WHERE Pardot_Prospect_Id__c IN : ProspectsByLawItem.get(li.Id) ]){
                Boolean includeComments =false;
                String pardotId = con.Pardot_Prospect_Id__c;
                //// for list size of 1
                for(Mailing_Subscription__c ms : con.Mailing_Subscriptions__r){
                    system.debug('found match with subscription ' + ms);
                    System.debug('>>>>>on.Mailing_Subscriptions__r: ' + con.Mailing_Subscriptions__r);
                    LawItems2ProsFiltered.get(li.Id).add(pardotId);
                }
            }
        }

        system.debug('LawItems2ProsFiltered '+ LawItems2ProsFiltered);
        system.debug('ProspectsByLawItem '+ ProspectsByLawItem);
        if(opperation.equalsIgnoreCase('pbl')){
            return LawItems2ProsFiltered;
        }

        ////// mapping ProspectsByLawItem END

        //// mapping items by prospects START
        for(String prospectId : allRelevantProspects.KeySet() ){
            for(String itemLawId : ProspectsByLawItem.KeySet()){
                if(ProspectsByLawItem.containsKey(itemLawId) && ProspectsByLawItem.get(itemLawId).contains(prospectId)){
                    if(!LawItemsPerProspects.containsKey(prospectId)){
                        LawItemsPerProspects.put(prospectId, new Set<String>());
                    }
                    LawItemsPerProspects.get(prospectId).add(itemLawId);
                }
            }
        }

        system.debug('LawItemsPerProspects '+ LawItemsPerProspects);
        if(opperation.equalsIgnoreCase('lbp')){
            return ProspectsByLawItem;
        }

        return null;
    }

    public static String convertKNSIdToString(Decimal KNSId){
        return String.valueOf(KNSId).removeEnd('.0');
    }

    public static Date convertStringToDate(String dString){
        try {
            List<String> splitted = dString.split('-');
            Integer year = Integer.valueOf(splitted[0]);
            Integer month = Integer.valueOf(splitted[1]);
            Integer day = Integer.valueOf(splitted[2].split('T')[0]);
            Date d = Date.newInstance(year, month, day);
            return d;
        } catch (Exception ex){
            return null;
        }
    }

    //1953-09-03T00:00:00
}