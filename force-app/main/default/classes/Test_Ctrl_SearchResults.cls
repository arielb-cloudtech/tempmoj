@isTest
public with sharing class Test_Ctrl_SearchResults {
    
    @isTest
    static void testSearch(){
        ClsObjectCreator cls = new ClsObjectCreator();
        Account acc = cls.createAccount('Main');
        Datetime publicationDate =  Datetime.now().addDays(-1);

        List<Law__c> laws = new List<Law__c> {
            new Law__c(LawID__c = '123', Law_Name__c ='parent1'),
            new Law__c(LawID__c = '457', Law_Name__c ='parent2')
        };
        insert laws;

        List<Law_Item__c> lawItems = new List<Law_Item__c> {
            new Law_Item__c(Status__c =    'Canceled', Publish_Date__c= publicationDate, Law_Item_Name__c = 'Law Item 1 abc', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= publicationDate, Law_Item_Name__c = 'Law Item 2 abc', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= publicationDate, Law_Item_Name__c = 'Law Item 3', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= publicationDate, Law_Item_Name__c = 'Law Item 4', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= publicationDate, Law_Item_Name__c = 'Law Item 5', Account_Responsibility__r = acc)
        };
        insert lawItems;

        List<Related_to_Law__c> lawRelations  = new List<Related_to_Law__c> {
            new Related_to_Law__c(Law__c = laws[0].Id, Law_Item__c= lawItems[0].Id),
            new Related_to_Law__c(Law__c = laws[0].Id, Law_Item__c= lawItems[1].Id),
            new Related_to_Law__c(Law__c = laws[1].Id, Law_Item__c= lawItems[4].Id)
        };
        insert lawRelations;

        List<Comment__c> comments = new List<Comment__c>{
            new Comment__c(Comment_Text__c='Comment 1', Related_to_Law_Item__c=lawItems[0].Id, Comment_Status__c = 'Published', Display_Comment__c = false),
            new Comment__c(Comment_Text__c='Comment 1', Related_to_Law_Item__c=lawItems[0].Id, Comment_Status__c = 'Published', Display_Comment__c = true)
        };
        insert comments;


        Map<Id, Integer> commentCounter = Ctrl_SearchResults.getCommentCount(new Set<id>{ lawItems[0].Id });
        System.assertEquals(1, commentCounter.get(lawItems[0].Id));
        
    }

    @TestSetup
    static void makeData(){
        //data creation class instance
        ClsObjectCreator cls = new ClsObjectCreator();
        //accounts
        List<Account> accList = new List<Account>();
        accList.add(cls.returnAccount('name1'));
        insert accList[0];
        //laws
        List<Law__c> laws = new List<Law__c>();
        laws.add(cls.returnLaw());
        laws.add(cls.returnLaw());
        laws.add(cls.returnLaw());
        insert laws;
        //law items
        List<Law_Item__c> liList = new List<Law_Item__c>();
        liList.add(cls.returnLawItem('Draft', 'wow1p', accList[0].Id));
        liList[0].RecordTypeId = [Select Id From RecordType Where DeveloperName = 'Memorandum_of_Law' Limit 1].Id;
        liList[0].Document_Brief_Short_1__c = 'wow1p';
        liList.add(cls.returnLawItem('Draft', 'wow2p', accList[0].Id));
        liList.add(cls.returnLawItem('Draft', 'wow3p', accList[0].Id));
        liList.add(cls.returnLawItem('Draft', 'wow4p', accList[0].Id));
        insert liList;
        //Classifications
        List<Classification__c> classifications = new List<Classification__c>();
        classifications.add(cls.returnClassification());
        insert classifications;
        //law classification
        List<LawClassification__c> lcList = new List<LawClassification__c>();
        lcList.add(cls.returnLC(classifications[0].Id, laws[0].Id));
        insert lcList;
        //law item classification
        List<Law_Item_Classification__c> licList = new List<Law_Item_Classification__c>();
        licList.add(cls.returnLIClassification(classifications[0].Id, liList[3].Id));
        insert licList;
        //related to law
        List<Related_to_Law__c> rtlList = new List<Related_to_Law__c>();
        rtlList.add(cls.returnRelatedToLaw(liList[3].Id, laws[0].Id));
        insert rtlList;

    }

    @isTest
    public static void test(){
        //instance of class
        FiltersWrapper fw = new FiltersWrapper();
        //record type ids
        RecordType memoRT = [Select Id From RecordType Where DeveloperName = 'Memorandum_of_Law' Limit 1];
        RecordType protRT = [Select Id From RecordType Where DeveloperName = 'Ministry_Directive_or_Procedure_Draft' Limit 1];
        RecordType testRT = [Select Id From RecordType Where DeveloperName = 'Support_Test_Draft' Limit 1];
        RecordType secRT = [Select Id From RecordType Where DeveloperName = 'Secondary_Legislation_Draft' Limit 1];
        fw.searchVar = 'wow';
        fw.rtChoices.numOfChoices = 1;
        fw.rtChoices.checkMemo = new FiltersWrapper.rtChoice();
        fw.rtChoices.checkMemo.Id = memoRT.Id; 
        fw.rtChoices.checkMemo.isSelected = true;
        fw.rtChoices.checkProt = new FiltersWrapper.rtChoice(); 
        fw.rtChoices.checkProt.Id = protRT.Id;
        fw.rtChoices.checkProt.isSelected = true;
        fw.rtChoices.checkTest = new FiltersWrapper.rtChoice(); 
        fw.rtChoices.checkTest.Id = testRT.Id;
        fw.rtChoices.checkTest.isSelected = true;
        fw.rtChoices.checkSec = new FiltersWrapper.rtChoice();
        fw.rtChoices.checkSec.Id = secRT.Id; 
        fw.rtChoices.checkSec.isSelected = true;
        List<FiltersWrapper.classification> lst1 = new List<FiltersWrapper.classification>();
        List<LawClassification__c> lst2 = [Select Id From LawClassification__c];
        for(Integer i=0; i< lst2.size();i++){
            lst1.add(new FiltersWrapper.classification());
            lst1[i].Id = lst2[i].Id;
        }
        fw.chosenClassifications = lst1;
        fw.distDateStart = String.valueOf(DateTime.newInstance(2019, 11, 11));
        fw.distDateEnd = String.valueOf(DateTime.newInstance(2019, 12, 11));

        //search type 1
        fw.orderBy = 'DESC';
        fw.openForComments = 'פתוח להערות ציבור';
        system.debug('jjjjjjjjjjjjjjj'+JSON.serialize(fw));
        //Ctrl_SearchResults.getLawItems(JSON.serialize(fw), '1');
        //search type 2
        fw.orderBy = 'ASC';
        fw.openForComments = 'סגור להערות ציבור';
        //Ctrl_SearchResults.getLawItems(JSON.serialize(fw), '2');
        //search type 3
        //Ctrl_SearchResults.getLawItems(JSON.serialize(fw), '3');
    }

}