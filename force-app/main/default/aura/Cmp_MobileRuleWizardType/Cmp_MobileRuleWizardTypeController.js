({
	closeFilters : function(cmp, evt, helper) 
	{
		if(cmp.get("v.chosenProcess") == '5')
		{
			cmp.set("v.inMain", "false");
			cmp.set("v.choice", "");
		}else
		{
			cmp.set("v.choice", cmp.get("v.chosenProcess"));
		}
	},

	next : function(cmp, evt, helper) 
	{
		try 
		{
			helper.saveRuleHelper(cmp, evt, helper);
		} catch (err) 
		{
			console.log("err: " + err.message);	
		}
	},

	doInit : function(cmp, evt, helper) 
	{
		var typeSelection = [];
		
		var i;
		for (i = 0; i <= 4; i++) 
		{ 
			typeSelection[i] = false;
		}

		cmp.set("v.typeSelection", typeSelection);
	},

	checkAll : function(cmp, evt, helper) 
	{
		var typeSelection = cmp.get("v.typeSelection");

		var counter = 0;
		for(const key in typeSelection)
		{
			if(typeSelection[key])
			{
				counter++;
			}
		}
		typeSelection[5] = counter;
		cmp.set("v.typeSelection", typeSelection);
	},
})