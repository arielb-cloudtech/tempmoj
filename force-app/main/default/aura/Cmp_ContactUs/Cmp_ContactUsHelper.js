({
    formFields:  function() { 
        return [
            //First_Name
            {
                label: $A.get('$Label.c.First_Name'),
                required: true,
                serverField: 'FirstName',
                value: '',
                type:'textField',
                class: 'halfLine first'
            }, 
            //Last_Name
            {
                label: $A.get('$Label.c.Last_Name'),
                required: true,
                serverField: 'LastName',
                value: '',
                type:'textField',
                class:'halfLine'
            }, 
            // Company
            {
                label: $A.get('$Label.c.Amuta_Company'),
                value: '',
                serverField: 'Company',
                type:'textField',
                class: 'halfLine first'
            }, 
            //Role
            {
                label: $A.get('$Label.c.Role'),
                value: '',
                serverField: 'Role',
                type:'textField',
                class:'halfLine'
            }, 
             // Email
           {
                label: $A.get('$Label.c.Email'),
                required: true,
                serverField: 'Email',
                type:'textField',
                class:'halfLine first',
                value: '',
                validation:[{
                    regex: /(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/,
                    message: $A.get("$Label.c.Invalid_email")
                }]
            },
            //Phone
            {
                label: $A.get('$Label.c.Phone'),
                serverField: 'Phone',
                type:'textField',
                class: 'halfLine',
                value: '',
                validation: [{
                    regex: /^\+?1?\s*?\(?\d{3}(?:\)|[-|\s])?\s*?\d{3}[-|\s]?\d{4}$/,
                    message: $A.get("$Label.c.Invalid_Phone_Number")
                }]
            }, 
            //Subject
            {
                label: $A.get('$Label.c.Feedback_Subject'),
                serverField: 'Subject',
                type:'textField',
                required: true,
                value: ''
            }, 
            //Message
            {
                label: $A.get('$Label.c.Feedback_content'),
                serverField: 'Message',
                type:'textArea',
                required: true,
                value: ''
            },
             //Files
            {
                type:'fileUpload'
            }, 
            
        ];
    },
    setCleanForm: function(cmp){
        var action = cmp.get("c.getCurrentUser");
        var helper = this;
        action.setCallback(this, function(response) {
            var state = response.getState();
            var formFields =  helper.formFields();
            if (state === "SUCCESS") {
                var user = response.getReturnValue();
                formFields.forEach(function(formField){
                    formField.value = user[formField.serverField];
                });
                console.log(formFields);
                cmp.set("v.formFields", formFields);
            }
        });
        $A.enqueueAction(action);
    }
});