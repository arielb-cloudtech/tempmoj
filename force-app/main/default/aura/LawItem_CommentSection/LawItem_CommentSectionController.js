({ 
	deleteComment: function (component, event, helper) {
		component.set('v.deleteComp', false);
		component.set('v.sectionNum', '');
		component.set('v.sectionComment', '');		
		var eve = $A.get("e.c:LawItem_CommentsEvent");
		console.log('eve = ' + eve);
		eve.fire();
	}
})