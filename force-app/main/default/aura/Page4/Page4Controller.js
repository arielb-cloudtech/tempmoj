({
	doValidation : function(cmp, event, helper){
		
		var lawItem = cmp.get("v.newLawItem");
		var radioValue = cmp.get("v.radioValue");
		var radioValueReason = cmp.get("v.radioValueReason");

		var filesForEditMode = cmp.get("v.filesForEditMode");
		var errorCounter = 0;

		if( !radioValue || (radioValue == "1" && !filesForEditMode.RIAFile) ){
			errorCounter++;
			cmp.set("v.showRIAFileError", "true");
		} else {
			cmp.set("v.showRIAFileError", "false");
		}

		if ( radioValue == "2" && radioValueReason == "2" && !lawItem.Five_Year_Plan_URL__c ) {
			errorCounter++;
			cmp.set("v.showHomeshError", "true");
		} else {
			cmp.set("v.showHomeshError", "false");
		}
		var GovDecChoice = cmp.get("v.GovDecChoice");
		if ( radioValue == "2" && radioValueReason == "3" && GovDecChoice == "נא לבחור סעיף" ) {
			errorCounter++;
			cmp.set("v.showGovDecError", "true");
		} else {
			cmp.set("v.showGovDecError", "false");
		}
		if ( radioValue == "2" && radioValueReason == "6" && !lawItem.No_RIA_Other_Reason__c ) {
			errorCounter++;
			cmp.set("v.showOtherError", "true");
		} else {
			cmp.set("v.showOtherError", "false");
		}

		return errorCounter == 0;
	},

	doInit : function(cmp, evt, helper) {
		var lawItem = cmp.get("v.newLawItem");
		var radioValue = cmp.get("v.radioValue");
		var radioValueReason = cmp.get("v.radioValueReason");
		var govDecChoice = cmp.get("v.GovDecChoice");
		var optionsReason3 = cmp.get("v.optionsReason3");

		switch(lawItem.RIA_Attachment__c){
			case 'Attach RIA Report':
				radioValue = '1';
				break;
			case 'RIA Report Not Required':
				radioValue = '2';
				break;
			default:
				radioValue = '';
		}

		switch(lawItem.No_RIA_Reason__c){
			case 'Unregulated':
				radioValueReason = '1';
				break;
			case 'Included in Five Year Plan':
				radioValueReason = '2';
				break;
			case 'Excluded by Government Decision':
				radioValueReason = '3';
				break;
			case 'Published before the Effective Date':
				radioValueReason = '4';
				break;
			case 'Negligible Impact':
				radioValueReason = '5';
				break;
			case 'Other':
				radioValueReason = '6';
				break;
			default:
			  radioValueReason = '';
		}
		
		var optionsReason32 = [];
		
		for(const key in optionsReason3){
			var opt = new Object();
			opt.label = optionsReason3[key].label;
			opt.isSelected = false;
			
			optionsReason32.push(opt);
		}

		switch(lawItem.Reason_for_Gov_Decision__c) {
			case 'Market Regulations':
				govDecChoice = 'רגולציות של שוק ההון, ביטוח וחסכון';
				govDecChoice = optionsReason32[0].label;
				optionsReason32[0].isSelected = true;
				break;
			case 'Imposing Fees':
				govDecChoice = 'מיסוי או הטלת אגרות או שינוי בשיעורן';
				govDecChoice = optionsReason32[1].label;
				optionsReason32[1].isSelected = true;
				break;
			case 'Social Services':
				govDecChoice = 'שירותים חברתיים';
				govDecChoice = optionsReason32[2].label;
				optionsReason32[2].isSelected = true;
				break;
			case 'Statutory Corporation':
				govDecChoice = 'תאגיד סטטוטורי';
				govDecChoice = optionsReason32[3].label;
				optionsReason32[3].isSelected = true;
				break;
			case '65% of Market Held by 3 Actors':
				govDecChoice = 'רגולציות שעניינן תחרות בשוק של עד שלושה שחקנים המחזיקים יחדיו מעל 65% מהשוק';
				govDecChoice = optionsReason32[4].label;
				optionsReason32[4].isSelected = true;
				break;
		}
		
		cmp.set("v.radioValue", radioValue);
		cmp.set("v.radioValueReason", radioValueReason);
		cmp.set("v.GovDecChoice", govDecChoice);
		cmp.set("v.optionsReason3", optionsReason32);
	},

	deleteFile : function(cmp, event, helper) {
		var deleteType = event.target.getAttribute("data-fileSection");
		var filesForEditMode = cmp.get("v.filesForEditMode");
		
		var action = cmp.get("c.deleteFileServer");
		action.setParams({ objId : filesForEditMode[deleteType].Id });
		action.setCallback(this, function(response) {
			var status = response.getState();
			if(cmp.isValid() && status === "SUCCESS") {
				filesForEditMode[deleteType] = null;
				cmp.set("v.filesForEditMode", filesForEditMode);
			}
		});
		$A.enqueueAction(action);
	},

	onMainRadioChange : function(cmp, evt, helper){
		helper.resetFieldsAndFiles(cmp);

		var lawItem = cmp.get("v.newLawItem");
		var radioValue = cmp.get("v.radioValue");

		switch(radioValue) {
			case '1':{
				lawItem.No_RIA_Reason__c = undefined; 
				lawItem.RIA_Attachment__c = 'Attach RIA Report';
				lawItem.X2118_Regulations__c = true;
				cmp.set("v.radioValueReason", "");
				break;
			} case '2': {
				lawItem.No_RIA_Reason__c = 'Unregulated'; 
				lawItem.RIA_Attachment__c = 'RIA Report Not Required';
				lawItem.X2118_Regulations__c = false;
				cmp.set("v.radioValueReason", "1");
				break;
			} default: {
				lawItem.RIA_Attachment__c = '';
				lawItem.No_RIA_Reason__c = undefined;		
			}
		}
		cmp.set("v.newLawItem", lawItem);
	},

	onSecRadioChange : function(cmp, evt, helper){
		try {
			helper.resetFieldsAndFiles(cmp);
			var lawItem = cmp.get("v.newLawItem");
			var radioValueReason = cmp.get("v.radioValueReason");

			switch(radioValueReason) {
				case '1':
					lawItem.No_RIA_Reason__c = 'Unregulated';
					break;
				case '2':
					lawItem.No_RIA_Reason__c = 'Included in Five Year Plan';
					break;
				case '3':
					lawItem.No_RIA_Reason__c = 'Excluded by Government Decision';
					console.log(cmp.get("v.optionsReason3"));
					break;
				case '4':
					lawItem.No_RIA_Reason__c = 'Published before the Effective Date';
					break;
				case '5':
					lawItem.No_RIA_Reason__c = 'Negligible Impact';
					break;
				case '6':
					lawItem.No_RIA_Reason__c = 'Other';
					break;
				default:
					lawItem.No_RIA_Reason__c = '';
			}
			cmp.set("v.newLawItem", lawItem);
		} catch (error) {
			console.log('ERROR:', error.message);

		}
	},

	govDecChange : function(cmp, evt, helper){
		var lawItem = cmp.get("v.newLawItem");
		var govDecChoice = cmp.get("v.GovDecChoice");
		switch(govDecChoice) {
			case 'רגולציות של שוק ההון, ביטוח וחסכון':
				lawItem.Reason_for_Gov_Decision__c = 'Market Regulations';
				break;
			case 'מיסוי או הטלת אגרות או שינוי בשיעורן':
				lawItem.Reason_for_Gov_Decision__c = 'Imposing Fees';
				break;
			case 'שירותים חברתיים':
				lawItem.Reason_for_Gov_Decision__c = 'Social Services';
				break;
			case 'תאגיד סטטוטורי':
				lawItem.Reason_for_Gov_Decision__c = 'Statutory Corporation';
				break;
			case 'רגולציות שעניינן תחרות בשוק של עד שלושה שחקנים המחזיקים יחדיו מעל 65% מהשוק':
				lawItem.Reason_for_Gov_Decision__c = '65% of Market Held by 3 Actors';
				break;
			default:
				lawItem.Reason_for_Gov_Decision__c = '';
		}
		cmp.set("v.newLawItem", lawItem);
	},

	markRIAFile : function(cmp,evt,helper){
		var uploadedFiles = evt.getParam("files");
		helper.changeFile(cmp, "RIAFile", uploadedFiles[0], function() {
			var lawItem = cmp.get("v.newLawItem");
			lawItem.RIA_File_URL__c = "/" + uploadedFiles[0].documentId;
			lawItem.RIA_File_Name__c = uploadedFiles[0].name;
			cmp.set("v.newLawItem", lawItem);
		});
	},

	updateNameFile2 : function(cmp, evt, helper){
		var uploadedFiles = evt.getParam("files");
		helper.changeFile(cmp, "HomeshFile", uploadedFiles[0],  function() {
			var lawItem = cmp.get("v.newLawItem");
				lawItem.Five_Year_Plan_File_Name__c = uploadedFiles[0].name;
				cmp.set("v.newLawItem", lawItem);
		});
	},

	updateNameFile3 : function(cmp, evt, helper){
		var uploadedFiles = evt.getParam("files");
		helper.changeFile(cmp, "NegligibleEffectFile", uploadedFiles[0], function() {
			var lawItem = cmp.get("v.newLawItem");
			lawItem.Negligible_Impact_Name__c =uploadedFiles[0].name;
			cmp.set("v.newLawItem", lawItem);
		});
	},

	updateNameFile4 : function(cmp, evt, helper){
		var uploadedFiles = evt.getParam("files");
		helper.changeFile(cmp, "OtherFile", uploadedFiles[0], function() {
			var lawItem = cmp.get("v.newLawItem");
			lawItem.Negligible_Impact_Name__c = uploadedFiles[0].Name;
			lawItem.No_RIA_Other_Name__c = uploadedFiles[0].name;
			cmp.set("v.newLawItem", lawItem);
		});
	}
})