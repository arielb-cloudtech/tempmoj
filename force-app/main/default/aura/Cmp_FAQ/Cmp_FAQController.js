({
	doInit : function(cmp, evt, helper) 
	{
		try 
		{
			var lang = location.search.substring(location.search.indexOf("language=")+9, location.search.indexOf("language=") +11);
			var action = cmp.get("c.getFAQData");
			action.setCallback(this, function(response) {
				var status = response.getState();
				if (cmp.isValid() && status === "SUCCESS") {
					var res = response.getReturnValue();
					var qaList = [];
					var i = 0;
					for(const key in res.Page_Childs__r)
					{
						var q = new Object();
						if(lang == 'iw' || !lang)
						{
							q.question = res.Page_Childs__r[key].Header_Hebrew__c;
							q.answer = res.Page_Childs__r[key].Body_Hebrew__c;
							q.open = false;
						}else if(lang == "ar")
						{
							q.question = res.Page_Childs__r[key].Header_Arabic__c;
							q.answer = res.Page_Childs__r[key].Body_Arabic__c;
							q.open = false;
						}
						qaList.push(q);
						i++;
					}
					cmp.set("v.qaList", qaList);
				}
			});
			$A.enqueueAction(action);
		} catch (err) 
		{
			console.log("Err:");	
			console.log(err.messsage);	
		}
	},
	openTab : function(cmp, evt, helper) 
	{
		console.log('openTab');
		var clicked = evt.target.parentNode.parentNode.classList[1];
		console.log('openTab ' + clicked);
		if(!clicked.includes("qNum"))
		{
			clicked = evt.target.parentElement.classList[1];
		}

		var qaList = cmp.get("v.qaList");
		qaList[clicked.substring(clicked.length-1)].open = !qaList[clicked.substring(clicked.length-1)].open;
		cmp.set("v.qaList", qaList);
			
	},
    showSearchIcon: function(cmp, evt, helper) {
    	var s = evt.getParam("showSearch");  
        cmp.set('v.showSearchEv', s);
	}
})