({
	doInit : function(cmp, evt, helper) 
	{
		try 
		{
			var rule = cmp.get("v.rule");
			console.log("rule: ");
			console.log(rule.ClassificationName);
			console.log(rule.Item_Type__c);
			console.log(rule.LawName);
			console.log(rule.MinistryName);
			var listing = [];
			if(rule)
			{
				if(rule.Item_Type__c)
				{
					var lineStr = $A.get("$Label.c.Document_Type_Search_Page") + ": " + rule.Item_Type__c;
					listing.push(lineStr);
				}

				if(rule.ClassificationName)
				{
					var lineStr = $A.get("$Label.c.Subject_Search_Page") + ": " + rule.ClassificationName;
					listing.push(lineStr);
				}
				
				if(rule.LawName)
				{
					var lineStr = $A.get("$Label.c.Related_to_the_law") + ": " + rule.LawName;
					listing.push(lineStr);
				}
				
				if(rule.MinistryName)
				{
					var lineStr = $A.get("$Label.c.Distributor_Office") + ": " + rule.MinistryName;
					listing.push(lineStr);
				}
			}
			cmp.set("v.ruleListing", listing);
		} catch (err) 
		{
			console.log("err.message");
			console.log(err.message);
		}
		

	},

	removeRule : function(cmp, evt, helper)
	{
		try 
		{
			var indx  = cmp.get("v.index");
			var ruleLst = cmp.get("v.ruleLst");
			console.log(ruleLst[0].Id);
			var ruleToDelId = ruleLst[indx].Id;

			var action = cmp.get("c.delRule");
				action.setParams({ ruleId : ruleToDelId });
				action.setCallback(this, function(response) {
					console.log('calling init rules');
					var status = response.getState();
					if(cmp.isValid() && status === "SUCCESS") 
					{
						console.log('res:');
						console.log(response.getReturnValue());
						ruleLst.splice(indx, 1);
						cmp.set("v.ruleLst", ruleLst);
					}

				});
				
				$A.enqueueAction(action); 
		} catch (err) 
		{
			console.log(err.message);
		}
		
	},
	editRule : function(cmp, evt, helper)
	{
		try 
		{
			var indx  = cmp.get("v.index");
			var ruleLst = cmp.get("v.ruleLst");
			cmp.set("v.isWizardEdit", "true");
			cmp.set("v.editIndex", indx);
			cmp.set("v.showWizard", "true");
		} catch (err) 
		{
			console.log(err.message);
		}
		
	},
})