({
    openSubMenu: function (cmp, event, helper) {
        var clickedMenu = event.target.getAttribute('data-menu');
        if( clickedMenu == cmp.get('v.openMenu') ) {
            cmp.set( 'v.openMenu', "" );
        }
        else {
            cmp.set( 'v.openMenu', clickedMenu );
        }
    },
    switchLanguage: function (cmp, event, helper) {
        event.preventDefault();
        var newLanguage = event.target.getAttribute("data-language");
        if(window.location.href.indexOf("language=")>-1) {
            window.location.href = window.location.href.replace(/language=../,'language='+newLanguage);
        }
        else {
            if(window.location.href.indexOf('?')> -1) {
                window.location.href = window.location.href+"&language="+newLanguage;
            }
            else {
                window.location.href = window.location.href+"?language="+newLanguage;
            }
        }
    },

    doNavigation: function (cmp, event, helper) {
        var linkElement = event.target;
        cmp.set("v.openMenu", "" );

        while( true ) {
            if(linkElement.href) {
                window.location = linkElement.href;
            }
            if( !linkElement || ! linkElement.parentElement ) {
                break;
            }
            linkElement = linkElement.parentElement;
        }
    },
   

    updateUnreadIndication: function (cmp, event, helper) {
        cmp.set('v.numOfUnreadComments', event.getParams().newUnreadCommentCount);
    },
    init: function (cmp, event, helper) {
        var menuSubItems = [
            { text: $A.get("$Label.c.What_is_the_Government_s_Relationships_Site") , href:'/s/about'},
            { text: $A.get("$Label.c.What_is_a_memo_Law_Title_On_Site") , href:'/s/about#C1'},
            { text: $A.get("$Label.c.What_is_a_Draft_of_Sublegislation") , href:'/s/about#C2'},
            { text: $A.get("$Label.c.What_is_a_Procedure_or_Guidance_Title_On_Website") , href:'/s/about#C3'},
            { text: $A.get("$Label.c.What_is_a_Support_Test_Title_On_Website") , href:'/s/about#C4' , isActive: true},
            { text: $A.get("$Label.c.Answers_and_Questions_Title_Page") , href:'/s/faq'},
            { text: $A.get("$Label.c.Contact_us_Page") , href:'/s/contact-us'},
        ];

        cmp.set("v.MenuSubItems", menuSubItems );

        if(location.pathname == "/s/" || location.pathname == "/s" || location.pathname == "/s/moj-searchresults")
        {
            cmp.set("v.showSearch", false);
        }
        var opLang = {code:'ar',name:$A.get("$Label.c.Arabic")};
        if($A.get("$Locale.language")=='ar') {
            opLang = {code:'iw',name:$A.get("$Label.c.hebrew")};
        }
        cmp.set("v.opLanguage", opLang);
        
        var action = cmp.get("c.getSession");
        action.setCallback(this, function (response) {
            var state = response.getState();
            var currentUser = {};

            if (state === "SUCCESS") {
                if (response.getReturnValue() != null) {
                    // Authenticated User
                    var responseObj = response.getReturnValue();
                    if( responseObj.status  == "success" ) {
                        currentUser = responseObj.user;
                        cmp.set('v.currentUser', currentUser);
                        cmp.set('v.numOfUnreadComments', responseObj.numberOfUnreadReplies);
                    }
                } 
            }	
            var registerUserEvent = $A.get("e.c:Evt_getCurrentUserData"); 
            registerUserEvent.setParams({"currentUser":currentUser});
            registerUserEvent.fire();		
        });
        $A.enqueueAction(action);
    }
})