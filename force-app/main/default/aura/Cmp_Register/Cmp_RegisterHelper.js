({
    formFields: function( formType ){
        if(formType == 'login') {
            return {
                "Email": {
                    label: $A.get('$Label.c.Email'),
                    ariaLabel: 'שדה חובה',
                    required: true,
                    serverField: 'username',
                    allowBlur: true,
                    validation:[{
                        regex: /(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/,
                        message: $A.get("$Label.c.Invalid_email")
                    }]
                },
                "Password": {
                    label: $A.get('$Label.c.Password_Sign_in_Page'),
                    ariaLabel: 'v.alPass',
                    required: true,
                    type:'password',
                    serverField: 'password',
                    allowBlur:true,
                    validation: [],
                },
            }
        }
        return {
            "First_Name": {
                label: $A.get('$Label.c.First_Name'),
                ariaLabel: $A.get("$Label.c.Mandatory_field"),
                required: true,
                serverField: 'firstName',
                class: 'halfLine first',
                validation: [{
                    regex: /.{2}/,
                    message: $A.get("$Label.c.minimumTwoChars")
                }]
            },
            "Last_Name": {
                label: $A.get('$Label.c.Last_Name'),
                required: true,
                serverField: 'lastName',
                class:'halfLine',
                validation: [{
                    regex: /.{2}/,
                    message: $A.get("$Label.c.minimumTwoChars")
                }]
            },
            "Company": {
                label: $A.get('$Label.c.Amuta_Company'),
                serverField: 'title'
            },
            "Role": {
                label: $A.get('$Label.c.Role'),
                serverField: 'role'
            },
            "Phone": {
                label: $A.get('$Label.c.Phone'),
                ariaLabel: 'v.alPhone',
                serverField: 'phone',
                validation: [{
                    regex: /^\+?1?\s*?\(?\d{3}(?:\)|[-|\s])?\s*?\d{3}[-|\s]?\d{4}$/,
                    message: $A.get("$Label.c.Invalid_Phone_Number")
                }]
            },
            "Email": {
                label: $A.get('$Label.c.Email'),
                ariaLabel: 'v.alEmail',
                required: true,
                serverField: 'email',
                validation:[{
                    // regex: /(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/,
                    regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: $A.get("$Label.c.Invalid_email")
                }]
            },
            "Password": {
                label: $A.get('$Label.c.Password_Sign_in_Page'),
                ariaLabel: 'v.alPass',
                required: true,
                type:'password',
                serverField: 'password',
                validation: [
                    {
                        regex: /.{8}/,
                        message: $A.get("$Label.c.Password_Validation")
                    },

                    {
                        regex: /(^[\x20-\x7E]*$).*$/,
                        message: $A.get("$Label.c.Password_Validation")
                    }
                ]
            },
            "PasswordVerification": {
                label: $A.get('$Label.c.Password_verification_Page_Sign_up'),
                ariaLabel: 'v.alPassVer',
                type:'password',
                required: true,
                allowBlur:true,
                validation: [{
                    match:"Password",
                    message: $A.get("$Label.c.Password_Verification_Error")
                }]
            }
        };
    },

    /**
     * Check the form. and return the form data
     *
     * @param {string} cmp - The component from the controller. used for data retrieve
     *
     * return {array} user input data if form is valid or an empty array if not
     */
    validateFormAndGetValues: function(cmp) {
        var formFields = this.formFields( cmp.get("v.formType") );
        var fieldNames = Object.keys(formFields);
        var firstInvalid = null;
        var params = {};

        for( var i = 0; i < fieldNames.length; i++ ) {
            var fieldId = fieldNames[i];
            var field = cmp.find(fieldId);
            var fieldDate = formFields[fieldId];
            if ( fieldDate.serverField ) {
                params[fieldDate.serverField] =  field.get("v.value");
            }
            console.log('validateField: ' + field);
            if ( !this.validateField(field, fieldDate.validation, cmp) ){
                if ( !firstInvalid ){

                    firstInvalid = field;
                }
            }
        }

        if( firstInvalid ) {
            firstInvalid.focus();
            return [];
        }
        return params;
    },

    /***
     *  Checks validation of ai:textField component using validation rules (as set in helper formFields)
     *
     *  Return Boolean - is value valid
     ***/
    validateField : function(field, Validations, component) {
        var errors = [];

        if ( !field.get("v.value") ) {
            if ( field.get("v.required") ) {
                errors.push({ message: field.get("v.label") + ": " + $A.get("$Label.c.Mandatory_field")});
            }
        }
        else {
            if ( Validations ) {
                Validations.forEach(function(rgx){
                    if ( rgx.match ) {
                        var matchValue = component.find(rgx.match).get("v.value");
                        if ( field.get("v.value") != matchValue ) {
                            errors = [{ message: rgx.message }];
                        }
                    }
                    if ( rgx.regex ) {
                        var pattern = new RegExp(rgx.regex );
                        if( !pattern.test(field.get("v.value")) ){
                            errors = [{ message: rgx.message }];
                        }
                    }
                });
            }
        }

        field.set("v.errors", errors);
        return errors.length == 0;
    }
})