({
	getFilterCleanObject : function() {
		var filters = [
			{
				name : 'lawTypes',
				title : $A.get("$Label.c.Document_Type_Search_Page"),
				selectedStatusesDescription:'',
				items : []
			},{
				name : 'commentsStatus',
				title : $A.get("$Label.c.Response_Option_Search_Page"),
				selectedStatusesDescription:'',
				items : [
					{ id:'Open', l: $A.get("$Label.c.Open_For_Public_Comments_Checkbox_Search_Page"), ty:"checkbox", v:false}, 
					{ id:'Closed', l: $A.get("$Label.c.Closed_for_Public_Comments_Checkbox_Search_Page"), ty:"checkbox", v:false}
				]
			},
			{
				name : 'distDate',
				title : $A.get("$Label.c.Date_of_distribution_Search_page"),
				selectedStatusesDescription:'',
				items : [
					{l: $A.get("$Label.c.To"), ty:"dateRange", since:null, until:null }
				]
			}, 
			{
				name : 'classifications',
				title : $A.get("$Label.c.Subject_Search_Page"),
				selectedStatusesDescription:'',
				modalId: "classifications",
				modalTitle: $A.get("$Label.c.Filter_By_Subject_Search_Page"),
				itemsByFirstChar: [],
				selectableItems: [],
				selectedItems: [],
				selectedCounter: 0,
				items : [
					{ name:'ignore', l: $A.get("$Label.c.All_Subject_Checkbox"), ty:"checkbox", v:false },
					{ l: $A.get("$Label.c.Filter_By_Subject_Search_Page"), ty:"link" },
					{ ty:"selectedPlaceholder" },
					{ l: $A.get("$Label.c.More"), ty:"more" }
				]
			}, 
			{
				name : 'offices',
				title : $A.get("$Label.c.Office_Search_Page"),
				selectedStatusesDescription:'',
				modalId: "offices",
				modalTitle: $A.get("$Label.c.Filter_By_Office"),
				itemsByFirstChar: [],
				selectableItems: [],
				selectedItems: [],
				selectedCounter: 0,
				items : [
					{ name:'ignore', l: $A.get("$Label.c.All_Office_Checkbox_Search_Page"), ty:"checkbox", v:false },
					{ l:  $A.get("$Label.c.Filter_By_Office"), ty:"link" },
					{ ty:"selectedPlaceholder" },
					{ l: $A.get("$Label.c.More"), ty:"more" }
				]
			}, 
			{
				name : 'filter',
				title : $A.get("$Label.c.Other_Filter_Search_Page"),
				modalId: "relatedLaws",
				modalTitle: $A.get("$Label.c.Filter_By_Related_Law_Search_Page"),
				relatedLawCategory: '',
				relatedLawSearchString: '',
				selectableItems: [],
				selectedItems: [],
				relatedItemToLoad: [],
				selectedCounter: 0,
				skipMobileHeader: true,
				items : [
					{ id:'RIA_only', ty:"checkbox", l: $A.get("$Label.c.Only_Documents_that_Include_RIA_Checkbox_Search_Page"), v:false}, 
					{ l:  $A.get("$Label.c.Filter_By_Related_Law_Search_Page"), ty:"link" },
					{ ty:"selectedPlaceholder" },
					{ l: $A.get("$Label.c.More"), ty:"more" }
				]
			}
		];
		return filters;
	},
	waitingId: null,
	getRelatedLaws: function(cmp) 
	{
		var action = cmp.get("c.getRelatedLawsItems");
		var filtersObject = cmp.get("v.filtersObject");
		var currentFilter = filtersObject.filter( f=>f.name=='filter')[0];
		var relatedLawTopics = [];

		if ( currentFilter.relatedLawCategory ) {
			relatedLawTopics.push( currentFilter.relatedLawCategory );
		}

		action.setParams({
            search: currentFilter.relatedLawSearchString,
			lawClassifaction: relatedLawTopics,
			excludeIds: currentFilter.selectedItems.map( v=>v.Id )
        });
		
		action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
				var responseObj = response.getReturnValue();
				
				currentFilter.selectableItems = currentFilter.selectedItems.concat(responseObj.relatedLaws);
               
				cmp.set('v.filtersObject', filtersObject );
			}

		});

		var timeOutMilisecs = 1;
		if( this.waitingId ) {
			window.clearTimeout(this.waitingId);
			timeOutMilisecs = 1200;
		}

        this.waitingId = window.setTimeout($A.getCallback(function() {
            if (cmp.isValid()) {
                $A.enqueueAction(action);
            }
        }), timeOutMilisecs);
	},
	clearSectionFilters: function(cmp, sectionId) {
		var filtersObject = cmp.get('v.filtersObject');
		filtersObject.find(o=>o.modalId == sectionId)
					 .selectedItems
					 .forEach(function(cbItem){
						cbItem.checked = false;
					 });
		cmp.set('v.filtersObject', filtersObject);
	},
	refreshPage: function(cmp) {
		var filtersObject = cmp.get('v.filtersObject'); 
		this.rewriteUrl(cmp, filtersObject);
		cmp.set('v.filtersObject', filtersObject);
	},

	rewriteUrl: function(cmp, filtersObject , page) {
		var selectedFilters = this.filterGetSelectedValues(cmp, filtersObject);
		var queryString=[];

		if( page ) {
			selectedFilters.page = page;
		}
		
		Object.keys(selectedFilters).forEach( function(key) {
			if(typeof selectedFilters[key] == "string"){
				queryString.push(key+'='+selectedFilters[key]);
			}
			else if(typeof selectedFilters[key] == "boolean"){
				queryString.push(key);
			}
			else {
				if(selectedFilters[key].length == 1) {
					queryString.push(key+'='+selectedFilters[key][0]);
				}
				else {
					selectedFilters[key].forEach(function(val){
						queryString.push(key+'[]='+val);
					});
				}
			}
		});

		history.pushState("", "Search result", "?"+queryString.join('&'));
	
		this.doFilterSearch(cmp , selectedFilters);
	},

	setFilterObjectFromQueryString : function( cmp, filterObject ) {
		var queryStringParams = document.location.search.substr(1, 9999).split("&").map(v=>v.split("="));
		
		queryStringParams.forEach(function(k){
			var key = k[0].replace("[]", "");
			key = key.replace("%5B%5D", "");
			var currentFilter = filterObject.find(f=> (f.modalId ==key ||  f.name ==key));
			if(currentFilter){
				if ( currentFilter.selectableItems && currentFilter.selectableItems.find(i=>i.Id==k[1]) ) {
					currentFilter.selectableItems.find(i=>i.Id==k[1]).checked = true;
				}
				else {
					var item = currentFilter.items.find(v=>v.id==k[1]);
					if(item) {
						currentFilter.items.find(v=>v.id==k[1]).v = true;
					}
				}
			}
			if ( key=="relatedLaws" ) {
				filterObject.filter(f=>f.name=='filter')[0].relatedItemToLoad.push(k[1]);
			}
			if ( key=="RIA_included" ) {
				filterObject.filter(f=>f.name=='filter')[0].items.find(i=>i.id='RIA_only').v = true;
			}
			if(key=="search") {
				cmp.set("v.searchString", decodeURIComponent(k[1]).replace(/\+/gi," "));
			}
			
		});
		//cmp.set("v.filtersObject", filtersObject)
	},

	filterGetSelectedValues : function( cmp, filtersObject ) {
		var selectedFilters = {};
		filtersObject.forEach( function(filtersSection) {
			var sectionValues = [];
			if ( filtersSection.selectedStatusesDescription != null ) {
				filtersSection.selectedStatusesDescription = [];
			}

			if( filtersSection.modalId ) {
				filtersSection.selectedItems = filtersSection.selectableItems.filter(i=>i.checked);
				filtersSection.selectedCounter = filtersSection.selectedItems.length;
				
				if ( filtersSection.selectedCounter > 0 ) {
					selectedFilters[filtersSection.modalId] = filtersSection.selectedItems.map( i=>i.Id );
					filtersSection.selectedStatusesDescription = filtersSection.selectedItems.map( i=>i.Name );
				}
				else {
					selectedFilters[filtersSection.modalId] = [];
				}
			}
			else {
				filtersSection.items.forEach( function(filtersItem){
					if ( filtersItem.ty == 'checkbox' ) {
						if ( filtersItem.v ) {
							sectionValues.push(filtersItem.id);
							filtersSection.selectedStatusesDescription.push( filtersItem.l);
						}
					}
					if ( filtersItem.ty == 'dateRange' ) {
						['since','until'].forEach(function(key){
							if(filtersItem[key]) {
								selectedFilters[key] = filtersItem[key];
								filtersSection.selectedStatusesDescription.push(filtersItem[key]);
							}
						}) 
					}
				});
			}
			
			if( sectionValues.length>0 ) {
				selectedFilters[filtersSection.name] = sectionValues;
			}

		});
		// build selected description labels
		filtersObject.forEach( function(filtersSection) {
			if( ! filtersSection.selectedStatusesDescription ) return;
			if( filtersSection.selectedStatusesDescription.length == 0 ) {
				filtersSection.selectedStatusesDescription = $A.get("$Label.c.All_Search_Page");
				return;
			}
			if( filtersSection.name == 'distDate' ) {
				filtersSection.selectedStatusesDescription = 	(filtersSection.items[0].since ? filtersSection.items[0].since  :'') + 
																' - ' + 
																(filtersSection.items[0].until ? filtersSection.items[0].until  :'');
				return;
			}
			filtersSection.selectedStatusesDescription = filtersSection.selectedStatusesDescription.join(',');
			
		});


		var RIA_only = filtersObject.filter(f=>f.name=='filter')[0].items.find(i=>i.id='RIA_only').v;
		if( RIA_only ) {
			selectedFilters.RIA_included = true;
		}
		selectedFilters.search = cmp.get("v.searchString");

		return selectedFilters;
	},
	doFilterSearch : function( cmp, selectedFilters ) {
		cmp.set("v.showSpinner", "true");
		var action = cmp.get("c.getLawItems");
		selectedFilters.sortBy = cmp.get('v.sortBy');
		selectedFilters.sortOrder = cmp.get('v.sortOrder');
		action.setParams(selectedFilters);
		
		action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
				var responseObj = response.getReturnValue();

				var returnItems = responseObj.laws; 	
				var filtersObject = cmp.get('v.filtersObject');

				for (var k in responseObj.counters) {
					if(k!='total'){
						filtersObject.find(s=>s.name=='lawTypes').items.find(li=>li.id==k).counter = responseObj.counters[k];
					}
				}
				
				var pagerObject = [];
				if( responseObj.pager.of > 1 ) {
					
					pagerObject.push({text:'', page: 1, class:'firstPage'+ (responseObj.pager.page ==1?' disabled':'')});
					pagerObject.push({text:'', page: (responseObj.pager.page > 1? (responseObj.pager.page-1):1), class:'prevPage'+ (responseObj.pager.page ==1?' disabled':'')});
					
					for ( var i=1; i<=responseObj.pager.of; i++ ) {
						if ( i == responseObj.pager.page ) {
							pagerObject.push({text:i, class:'current' , title:''});
						}
						else if( i<4 || i> responseObj.pager.of - 3 ) {
							pagerObject.push({text:i, page: i, class:'pagerLink'});
						}
						else if(pagerObject[pagerObject.length-1].class!='separator') {
							pagerObject.push({text:'...', class:'separator' });
						}

					}
					console.log(responseObj.pager.page);
					pagerObject.push({text:'', page: (responseObj.pager.page+1) , class:'nextPage'+ (responseObj.pager.page == responseObj.pager.of?' disabled':'')});
					pagerObject.push({text:'', page: responseObj.pager.of, class:'lastPage'+ (responseObj.pager.page == responseObj.pager.of?' disabled':'')});
					
				}

				cmp.set('v.pagerLinks', pagerObject);

				cmp.set('v.filtersObject', filtersObject);
  
				cmp.set("v.resultCounters", response.getReturnValue().counters);	
				
				cmp.set("v.lawItems", returnItems);		
			}
			else {
				console.log( response.getError());
			}
			cmp.set("v.showSpinner", "false");
			scroll(0,0);
		});
		
		$A.enqueueAction(action);
	},
})