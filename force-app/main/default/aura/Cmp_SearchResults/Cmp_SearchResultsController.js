({
	doInit : function(cmp, event, helper) 
	{
		// create classifications (recTypes) checkboxs

		var filtersObject = helper.getFilterCleanObject();
		helper.setFilterObjectFromQueryString( cmp , filtersObject );
		
		// fetch filter values
		var actionGetFilters = cmp.get("c.getFilters");
		
		actionGetFilters.setParams({ relatedLaws: filtersObject.filter(f=>f.name=='filter')[0].relatedItemToLoad });

		actionGetFilters.setCallback(this, function(response) {
			if(response.getState() == "SUCCESS") {
				var responseObj = response.getReturnValue();
				
				filtersObject.filter(s=>s.name=='lawTypes')[0].items = responseObj.lawTypes.map(s=>({v:false, ty:"checkbox",id:s.Id, l:s.Name}));
				
				filtersObject.filter(s=>s.name=='filter')[0].related_law_topics = responseObj.related_law_topics;
				filtersObject.filter(f=>f.name=='filter')[0].selectableItems = responseObj.related_laws;
				filtersObject.filter(s=>s.name=='filter')[0].selectedItems.forEach(v => v.checked = true);

				filtersObject.filter(s=>['classifications','offices'].indexOf(s.name) > -1 )
							 .forEach( function(filtersObjectSection) {
					var filterResponseItems = responseObj[filtersObjectSection.name];
					filtersObjectSection.itemsByFirstChar = [];
					if(! filterResponseItems ) return;
					filterResponseItems.sort((a, b) => a.Name > b.Name ? 1 : -1);
					var firstChar = "";
					filterResponseItems.forEach(function(classificationItem) {
						if ( firstChar!= classificationItem.Name[0] ) {
							firstChar = classificationItem.Name[0];
							filtersObjectSection.itemsByFirstChar.push({ 'firstChar': firstChar, 'items': [] });
						}
						classificationItem.checked = false;
						filtersObjectSection.itemsByFirstChar.find(v=>v.firstChar == firstChar).items.push(classificationItem);
						filtersObjectSection.selectableItems.push(classificationItem);
					});
				});
				
				helper.setFilterObjectFromQueryString( cmp , filtersObject );

				cmp.set('v.filtersObject', filtersObject );
				helper.refreshPage(cmp);
			}
		});

		$A.enqueueAction(actionGetFilters);
	},

	downloadToCsv : function(cmp, event, helper) {
		cmp.set("v.showSpinner", "true");
		var filtersObject = cmp.get('v.filtersObject');
		var selectedFilters = helper.filterGetSelectedValues(cmp, filtersObject);
		var action = cmp.get("c.getCSVSearchResults");
		action.setParams(selectedFilters);
		
		action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
				var csv = response.getReturnValue();          
				var hiddenElement = document.createElement('a');
				hiddenElement.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURI(csv);
				hiddenElement.target = '_self'; // 
				hiddenElement.download = 'searchResults.csv';  // CSV file Name* you can change it.[only name not .csv] 
				document.body.appendChild(hiddenElement); // Required for FireFox browser
				hiddenElement.click();
			}
			
			cmp.set("v.showSpinner", "false");

		});
		
		$A.enqueueAction(action);
	},

	closeModal : function(cmp, event, helper) {
		cmp.set("v.popup", "");
		helper.refreshPage(cmp);
	},

	gotoPage : function(cmp, event, helper) {
		var filtersObject = cmp.get("v.filtersObject");
		var selectedFilters = helper.filterGetSelectedValues(cmp, filtersObject);
		selectedFilters['page'] = event.target.getAttribute("data-pageIndex");
		helper.rewriteUrl(cmp, filtersObject , event.target.getAttribute("data-pageIndex"));
	},

	cancelModal : function(cmp, event, helper) {
		cmp.set("v.popup", "");
		var callerModalId = event.target.getAttribute("data-category");
		helper.clearSectionFilters(cmp, callerModalId);
	},
	
	clearModalFilters : function(cmp, event, helper) {
		var callerModalId = event.target.getAttribute("data-category");
		helper.clearSectionFilters(cmp, callerModalId);
	},

	showFilters : function(cmp, event, helper) {
		$A.util.addClass(cmp.find("searchPage"),'showFilters');
	},

	openFilterSection: function(cmp, event, helper) {
		var curElement = event.target;
		while(curElement) {
			if(curElement.classList.contains('filterSection')){
				curElement.classList.toggle('show');
				break;
			}
			curElement = curElement.parentElement;
		}
	},

	hideFilters: function(cmp, event, helper) {
		$A.util.removeClass(cmp.find("searchPage"),'showFilters');
	},
	filterRelatedLaws: function(cmp, event, helper) 
	{
		helper.getRelatedLaws(cmp);
	},
	filterChange : function(cmp, event, helper) 
	{
		helper.refreshPage(cmp);
	},

	checkRelatedLawItem: function(cmp, event, helper) 
	{
		helper.refreshPage(cmp);
    },
	clearFilters : function(cmp, event, helper) {
		var filters = cmp.get('v.filtersObject');
		filters.forEach(function(filter){
			filter.items.forEach(function(item){
				if ( item.ty == "checkbox" ) {
					item.v = false;
				}
				if ( item.ty == "dateRange") {
					item.since= null;
					item.until= null;
				}
			});
			if(filter.selectedItems){
				filter.selectedItems.forEach(function(item){
					item.checked = false;
				})
			}
		});
		cmp.set('v.filtersObject', filters );
		helper.refreshPage(cmp);
	},
	 changeSortOrder : function(cmp, event, helper) {
		var sortOrder = cmp.get("v.sortOrder");
		sortOrder = sortOrder == 'DESC'?'ASC':'DESC';
		cmp.set("v.sortOrder", sortOrder);
		helper.refreshPage(cmp);
	},
	changeSort : function(cmp, event, helper) {
		cmp.set("v.sortBy", event.target.value);
		helper.refreshPage(cmp);
	},
	
	openModal : function(cmp, event, helper) {
		var popForm = event.target.getAttribute('data-modal');
		if( popForm == 'relatedLaws' ) {
			helper.getRelatedLaws(cmp);
		}
		cmp.set("v.popup", popForm);
		scroll(0,0);
		setTimeout(function(){
			cmp.find("popupCloseButton").getElement().focus();
		},1000);
	}
})