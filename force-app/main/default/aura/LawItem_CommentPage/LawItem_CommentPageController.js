({
	doInit: function (cmp, event, helper) {		
		var action = cmp.get("c.getData");
		action.setParams({ lawItemId: cmp.get("v.recordId") });
		action.setCallback(this, function (response) {
			console.log('response.getState()', response.getState());
			var state = response.getState();
			//var elFile = document.getElementById('fileIcon');
			if (cmp.isValid() && state === "SUCCESS") {
				var returnValue = new Object();
				// returnValue = response.getReturnValue();
				returnValue = JSON.parse(JSON.stringify(response.getReturnValue()));
				console.log('returnValue 1', returnValue);
				console.log('returnValue 2', returnValue["mComments"]);
				console.log('tom is a beach', returnValue["mAllowToComment"]);

				cmp.set("v.comments", returnValue);

				console.log('DONE');
			}
		});
		$A.enqueueAction(action);
	},
	openModel: function (component, event, helper) {
		// for Display Model,set the "isOpen" attribute to "true"
		component.set("v.isSaving", false);
		component.set("v.isOpen", true);
	},

	closeModel: function (component, event, helper) {
		var eve = $A.get("e.c:LawItem_DeleteCreatedComment");
		eve.fire();
	},

	saveAndClose: function (component, event, helper) {
		//console.log('after param ' + component.get("v.recordId"));
		 component.set("v.isSaving", true);
		 var eve = $A.get("e.c:LawItem_SaveEvent");
		  eve.setParams({		 
		   "recordId" : component.get("v.recordId")
		   });		
		 eve.fire();		
		 //component.set("v.isOpen", false);		
	},
	savePopUp: function (component, event, helper) {
		component.set("v.isOpen2", true);	
	},
	closeSavePopUp: function (component, event, helper) {
		component.set("v.isOpen2", false);	
	},

	addToChatter: function(component, event, helper) {
		console.log("addToChatter ...");

		var commentMap = JSON.parse(JSON.stringify(component.get("v.commentIdMap")));

		if(commentMap != null && commentMap != undefined){
			var items = 0;
			Object.keys(commentMap).forEach(function(key){
				items++;
			});
			console.log("CommentMap len:" + items);
			if (items)
				component.set("v.addChater", true);
		}
	},

	closeAddChatter: function(component, event, helper) {
		console.log("closeAddChatter ..."); 
		component.set("v.addChater", false);
	},

	saveAddChatter: function(component, event, helper){
		console.log("~~~~~~saveAddChatter~~~~~~");
		console.log("find: ", component.find("lookupComponent"));
		var lookupComponent =  component.find("lookupComponent");
		var btnCmp =  lookupComponent.myMethod();
		if(btnCmp.get("v.validity").valid) {
		   	console.log("saveAddChatter ...");
			console.log("selectRecordId", component.get("v.selectRecordId"));

			var commentMap = JSON.parse(JSON.stringify(component.get("v.commentIdMap")));
			var commentList = [];
			Object.keys(commentMap).forEach(function(key){
				commentList.push(commentMap[key]);
			});

			console.log("saveAddChatter commentList:" + commentList);
			// component.set("v.addChater", false);
			var action = component.get("c.AddChatter");
			action.setParams({ "commentIDList": commentList, "userId": component.get("v.selectRecordId") });

			action.setCallback(this, function (response){
				console.log("addToChatter : response from c.AddChatter");
				console.log('addToChatter: response.getState()', response.getState());
				component.set("v.commentIdMap", null);
				component.set("v.textMap", null)
				var eve = $A.get("e.c:LawItem_CommentEvent");
				console.log("saveAddChatter eve:" + eve);
				eve.fire();
				component.set("v.selectRecordId", undefined);
				component.set("v.addChater", false);
			});
			$A.enqueueAction(action);
		} else {
		    btnCmp.showHelpMessageIfInvalid();
		}
	},

	downLoadSelected: function(component, event, helper) {
	console.log("downLoadSelected ...");
		
		var myMap = JSON.parse(JSON.stringify(component.get("v.textMap")));
		console.log("myMap :", myMap);
		console.log("myMap Len :", myMap.length);
		var concatText = '';
		Object.keys(myMap).forEach(function(key) {
			concatText += myMap[key] + "\n";
		});
		console.log("concatText : ", concatText);
		
		var action = component.get("c.downloadPdfFile");
		action.setParams({ fileBody: concatText });
		console.log('action: ' + action);
		action.setCallback(this, function (response) {
			console.log('response.getState()', response.getState());
			var state = response.getState();
			
			if (component.isValid() && state === "SUCCESS") {
				var returnValue = JSON.parse(JSON.stringify(response.getReturnValue()));
				console.log('returnValue', returnValue);
	
				var win = window.open(returnValue["mDownloadUrl"], '_blank');
				win.focus();
				// var action2 = component.get("c.deletePdfFile");
                // action2.setParams({ documentId: returnValue["mId"] });
                // $A.enqueueAction(action2);

				

				console.log('DONE');
			}
		});
		$A.enqueueAction(action);
	},



//------------------------------------------------------------------------------------------------------------------------------

	
	// clickOption: function (cmp, evt) 
	// {
	// 	var isEdit = cmp.get("v.isEdit");
	// 	var lawItem = cmp.get("v.newLawItem");
	// 	var clicked = evt.target.textContent;
	// 	cmp.set("v.searchVar", clicked);
	// 	cmp.set("v.lawChoice", evt.target.nextSibling.textContent);
	// 	lawItem.Law__c = evt.target.nextSibling.textContent;
	// 	if(isEdit == 'false')
	// 	{
	// 		lawItem.Law_Item_Name__c = "תזכיר " + clicked + " (תיקון מס' X) " + cmp.get("v.hebYear") + " - " + new Date().getFullYear();
	// 	}
	// 	cmp.set("v.newLawItem", lawItem);
	// 	var action = cmp.get("c.getUsers");
	// 	action.setParams({ parent : cmp.get("v.lawChoice") });
	// 	action.setCallback(this, function(response) {
	// 		var status = response.getState();
    //         if(cmp.isValid() && status === "SUCCESS") 
    //         {
	// 			cmp.set("v.childLaws", JSON.parse(response.getReturnValue()));
	// 			cmp.set("v.childLawsSearchRes", JSON.parse(response.getReturnValue()));
	// 		}
	// 	});
	// 	$A.enqueueAction(action);
	// },
})