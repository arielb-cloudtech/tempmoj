({
    doInit: function (cmp, event, helper) {
        var visibilityOptions = [
            {'label': $A.get("$Label.c.Government_offices_only_radio_Button"), 'value': 'private' },
            {'label': $A.get("$Label.c.All_user_website_radio_Button"), 'value': 'public' }
        ];
        cmp.set("v.visibilityOptions", visibilityOptions);
        var hash = window.location.hash.substr(1,20);
        if( ['unread', 'replies'].indexOf(hash)>-1 ) {
            cmp.set("v.ActiveFilter", hash);
        }
        helper.fetchComments(cmp);
    },

    doReload: function (cmp, event, helper) {
        helper.fetchComments(cmp);
    },

    getMore: function (cmp, event, helper) {
        helper.fetchComments(cmp);
    },
    
    handleCommentMarkAsRead: function (cmp, event, helper) {  
        
        var commentId = event.getParams().commentId;
        console.log(commentId);
        var currentFilter = cmp.get("v.ActiveFilter");
        
        var action = cmp.get('c.markCommentAsRead');

        action.setParams({  
            commentsToMark: commentId,
            filter: currentFilter
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(cmp.isValid() && state === 'SUCCESS'){
                console.log(commentId);
                var responseObj = response.getReturnValue();
                var comments = cmp.get("v.Comments");
                var comment =  comments.filter(c=>c.replies.find(r=>r.Id == commentId ))[0];
                if( comment ) { 
                    comment.replies.find(r=>r.Id==commentId).IsUnread = false;
                    comment.hasUnreadComment = comment.replies.find(c=>c.IsUnread)!=null;

                    var registerUserEvent = $A.get("e.c:Evt_updateUnreadCounter"); 
                    registerUserEvent.setParams({"newUnreadCommentCount":responseObj.Counters.unread });
                    registerUserEvent.fire();	

                    cmp.set( "v.Comments", comments );
                    cmp.set( "v.Counters", Object.entries(responseObj.Counters) );
                }
            } 
        });
        $A.enqueueAction(action);
    },

    editComment: function(cmp, event, helper) {
        var filter = cmp.get("v.ActiveFilter");
        if ( filter == 'drafts' ) {
            return;
        }
        event.preventDefault();
        var comments = cmp.get("v.Comments");
        var commentIndex = event.target.getAttribute("data-commentIndex");
        var comment = comments[commentIndex];
        if ( comment.privateLabel ) {
            helper.saveComment(cmp , comment.Id, comment.privateLabel == 'private' , commentIndex );
            comment.editMode = false;
        }
        else {
            comment.editMode = true;
            comment.privateLabel = comment.IsPrivate?'private':'public';
        }
        cmp.set("v.Comments", comments);
    },
    changeFilter: function(cmp, event, helper) {
        var elem = event.target;
        while( true ) {
            if( elem.hasAttribute('data-filter') ) {
                window.location.hash = elem.getAttribute('data-filter');
                cmp.set("v.ActiveFilter", elem.getAttribute('data-filter'));
                cmp.set("v.Comments", []);
                cmp.set("v.commentLeft", 0);
                helper.fetchComments(cmp);
                break;
            }
            if( !elem || ! elem.parentElement ) {
                break;
            }
            elem = elem.parentElement;
        }
    },
})