({
    saveComment: function(cmp , updatedCommentId, commentIsPrivate , editCommentIndex){
        var action = cmp.get("c.setCommentVisibility");
		
		action.setParams({
			commentId: updatedCommentId,
			isPrivate:commentIsPrivate
		});
		
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var returnValue = response.getReturnValue();
				if(returnValue.Comments.length == 1) {
                    var comments =  cmp.get("v.Comments");
                    comments[editCommentIndex] = returnValue.Comments[0];
                    cmp.set("v.Comments", comments);
                }
            }
        });
		$A.enqueueAction(action);
    },
    fetchComments: function(cmp){
        var action = cmp.get("c.getUserComments");
        var comments =  cmp.get("v.Comments");
        var filter = cmp.get("v.ActiveFilter");
        
        action.setParams({ 
            excludeIds:comments.map(s=>s.Id), 
            filter: filter
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if ( state === "SUCCESS" ) {
                var responseObj = response.getReturnValue();
                responseObj.Comments.forEach(comment=>comment.hasUnreadComment = comment.replies.find(c=>c.IsUnread)!=null);
                comments =  comments.concat( responseObj.Comments );
                cmp.set("v.Comments", comments);

                if ( filter == 'total' || filter == 'drafts' ) {
                    cmp.set("v.commentLeft", responseObj.Counters.total - comments.length );
                }
                else {
                    cmp.set("v.commentLeft", responseObj.Counters[filter] - comments.reduce((a,b)=>a.replies.length + b.replies.length));
                }

                cmp.set("v.Counters", Object.entries(responseObj.Counters) );
            }
        });
        $A.enqueueAction(action);
    }
})