({
    fetchComments: function(cmp){
        var action = cmp.get("c.getUserComments");
        var comments =  cmp.get("v.Comments");
        
        action.setParams({ 
            excludeIds:comments.map(s=>s.Id), 
            filter: 'drafts'
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if ( state === "SUCCESS" ) {
                var responseObj = response.getReturnValue();
                comments =  comments.concat( responseObj.Comments );
                cmp.set("v.Comments", comments);
                cmp.set("v.commentLeft", responseObj.Counters.total - comments.length );
            }
        });
        $A.enqueueAction(action);
    }
})