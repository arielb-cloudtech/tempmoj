({
	closeFilters : function(cmp, evt, helper) 
	{
		cmp.set("v.showWizard", "false");	
	},

	applyChoice : function(cmp, evt, helper) 
	{
		try
		{
			console.log("im here");
			cmp.set("v.showSpinner", "true");							
			var itmTypeChoices = cmp.get("v.itmTypeChoicesObj");
			if(evt.getParam("itmTypeChoices"))
			{
				itmTypeChoices = evt.getParam("itmTypeChoices");
			}
			var rtCounter = 0;
			for(const key in itmTypeChoices)
			{
				if(itmTypeChoices[key].isSelected == true)
				{
					rtCounter++;
				}
			}
			itmTypeChoices.numOfChoices = rtCounter;
			var startDate = cmp.get("v.startDate");
			var endDate = cmp.get("v.endDate");
			var chosenSubs = cmp.get("v.chosenSubs");
			var selectedLaws = cmp.get("v.selectedLaws");
			var selectedSecLaws = cmp.get("v.selectedSecLaws");
			var selectedOffices = cmp.get("v.selectedOffices");
			var openForComments = cmp.get("v.value");
			var ria = cmp.get("v.RIAFilter");
			var ordb = cmp.get("v.orderBy");
			if(ordb)
			{
				ordb = "Desc";
			}
			if(chosenSubs.length == 0)
			{
				chosenSubs = evt.getParam("classes");
			}
			console.log("chosenSubs: ");
			console.log(chosenSubs);
	
			var filterObj = new Object();
			filterObj.rtChoices = itmTypeChoices
			filterObj.distDateStart = startDate;
			filterObj.distDateEnd = endDate;
			filterObj.chosenClassifications = chosenSubs;
			filterObj.relatedLaws = selectedLaws;
			filterObj.relatedSecLaws = selectedSecLaws;
			filterObj.selectedOffices = selectedOffices;
			filterObj.openForComments = openForComments;
			filterObj.RIAFilter = ria;
			filterObj.orderBy = ordb;
			console.log(JSON.stringify(filterObj));
			
			console.log("searchval: ");
			// var searchVal = cmp.get("v.searchVal");
			var indx = window.location.search.indexOf("search4");
			var searchVal = "%" + decodeURIComponent(window.location.search.substring(indx).substring(8)).replace(/\+/g, " ") + "%";
			var indxType = window.location.search.indexOf("type");
        	var type = decodeURIComponent(window.location.search.substring(indxType).substring(5,6));
			filterObj.searchVar = searchVal;
			console.log(searchVal);
			var action = cmp.get("c.getLawItems");
			action.setParams({ filters : JSON.stringify(filterObj),
								searchType : type});
			action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
			
				console.log("i have returned");
				console.log(response.getReturnValue());
				if(response.getReturnValue() != "empty")
				{
					var returnValue = JSON.parse(response.getReturnValue()); 
					// cmp.set("v.LawItems", returnValue);
					console.log(returnValue.length);
					var processRes = $A.get("e.c:ProcessRes");
					processRes.setParams({lawItems : returnValue});
					console.log(processRes);
					processRes.fire();
					//cmp.set("v.showSpinner", "false");							
				}else
				{
					var lis = [];
					cmp.set("v.lawItems", lis);
					var processRes = $A.get("e.c:ProcessRes");
					processRes.setParams({lawItems : lis});
					console.log(processRes);
					processRes.fire();
				}

			}
			
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + 
									errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
			});
			cmp.set("v.isFiltersOpen", "false");
			$A.enqueueAction(action);
			// evt.pause();
		}catch(err)
		{
			console.log("error: ");
			console.log(err.message);
		}
		var filter = $A.get("e.c:runFilterDesktop");	
		//var filter = cmp.getEvent("filterRes");	
		filter.setParams({classes : cmp.get("v.chosenSubs")});
		console.log("filter");
		console.log(filter);
		//filter.fire();
		console.log("chosenSubsModal");
		console.log(cmp.get("v.chosenSubs"));
		cmp.set("v.isClassFilterOpen", "false");

	},
	clearChoice : function(cmp, evt, helper) 
	{
		var selectedOffices = [];
		var abcMap1 = cmp.get("v.abcMap1");
		var abcMap2 = cmp.get("v.abcMap2");
		var abcMap3 = cmp.get("v.abcMap3");
		for(const key in abcMap1)
		{
			for(const key2 in abcMap1[key].offices)
			{
				abcMap1[key].offices[key2].isSelected = false;
			}
		}
		for(const key in abcMap2)
		{
			for(const key2 in abcMap2[key].offices)
			{
				abcMap2[key].offices[key2].isSelected = false;
			}
		}
		for(const key in abcMap3)
		{
			for(const key2 in abcMap3[key].offices)
			{
				abcMap3[key].offices[key2].isSelected = false;
			}
		}
		cmp.set("v.abcMap1", abcMap1);
		cmp.set("v.abcMap2", abcMap2);
		cmp.set("v.abcMap3", abcMap3);
		cmp.set("v.chosenSubs", selectedOffices);
	},
	clearSpecChoice : function(cmp, evt, helper) 
	{
		//var abcMap = cmp.get("v.abcMap");
		var off = evt.getParam("cls");
		var abcMap1 = cmp.get("v.abcMap1");
		var abcMap2 = cmp.get("v.abcMap2");
		var abcMap3 = cmp.get("v.abcMap3");
		// console.log("abcMap1");
		// console.log(abcMap1);
		// console.log("abcMap2");
		// console.log(abcMap2);
		// console.log("abcMap3");
		// console.log(abcMap3);
		for(const key in abcMap1)
		{
			for(const key2 in abcMap1[key].offices)
			{
				// console.log("off");
				// console.log(off);
				// console.log("abcMap1[key].offices[key2].Name");
				// console.log(abcMap1[key].offices[key2].office.Name);
				if(abcMap1[key].offices[key2].office.Name == off && abcMap1[key].offices[key2].isSelected == true)
				{
					abcMap1[key].offices[key2].isSelected = false;
				}
			}
		}
		for(const key in abcMap2)
		{
			for(const key2 in abcMap2[key].offices)
			{
				// console.log("cls");
				// console.log(off);
				// console.log("abcMap1[key].offices[key2]");
				// console.log(abcMap2[key].offices[key2]);
				// console.log("abcMap1[key].offices[key2].Name");
				// console.log(abcMap2[key].offices[key2].office.Name);
				if(abcMap2[key].offices[key2].office.Name == off && abcMap2[key].offices[key2].isSelected == true)
				{
					abcMap2[key].offices[key2].isSelected = false;
				}
			}
		}
		for(const key in abcMap3)
		{
			for(const key2 in abcMap3[key].offices)
			{
				if(abcMap3[key].offices[key2].office.Name == off && abcMap3[key].offices[key2].isSelected == true)
				{
					abcMap3[key].offices[key2].isSelected = false;
				}
			}
		}
		cmp.set("v.abcMap1", abcMap1);
		cmp.set("v.abcMap2", abcMap2);
		cmp.set("v.abcMap3", abcMap3);
		
		//cmp.set("v.abcMap", abcMap);
		// cmp.set("v.selectedOffices", selectedOffices);
	},

	doInit : function(cmp, evt, helper) 
	{
		console.log('in classes init filters');
		var action = cmp.get("c.getOffices");
		action.setCallback(this, function(response) {
			var STATE = response.getState();
			console.log("state???");
			console.log(STATE);
			if(STATE === "SUCCESS") {
				console.log(response.getReturnValue());
				var returnValue = JSON.parse(response.getReturnValue()); 
				console.log(returnValue);
				for(const key in returnValue)
				{
					for(const key2 in returnValue[key].offices)
					{
						returnValue[key].offices[key2].isSelected = false;
					}
				}
				console.log(returnValue);
				cmp.set("v.abcMap", returnValue);
				cmp.set("v.abcMap1", returnValue.slice(0,5));
				cmp.set("v.abcMap2", returnValue.slice(5,11));
				cmp.set("v.abcMap3", returnValue.slice(11,returnValue.length));
			}
			
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + 
									errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
	
		$A.enqueueAction(action);
	},

	chooseOffice : function(cmp, evt, helper) 
	{
		try
		{
			console.log("evt");
			console.log(evt.target.classList[0]);
			console.log(evt.getSource());
			var clickedId = evt.target.classList[0];
			var abcMap = cmp.get("v.abcMap");
			var abcMap1 = cmp.get("v.abcMap1");
			var abcMap2 = cmp.get("v.abcMap2");
			var abcMap3 = cmp.get("v.abcMap3");
			var selectedIds = [];
			var selectedOffices = cmp.get("v.chosenSubs");
			console.log("sel offices length");
			console.log(selectedOffices.length);
			
			for(const key in selectedOffices)
			{
				selectedIds.push(selectedOffices[key].Id);
			}

			for(const key in abcMap)
			{
				for(const key2 in abcMap[key].offices)
				{
					//console.log(abcMap[key].offices[key2]);
					if(abcMap[key].offices[key2].isSelected == true && !selectedIds.includes(abcMap[key].offices[key2].office.Id))
					{
						var selectedOffice = new Object();
						selectedOffice.Id = abcMap[key].offices[key2].office.Id;
						selectedOffice.Name = abcMap[key].offices[key2].office.Name;
						selectedOffice.label = abcMap[key].offices[key2].office.Name;
						selectedOffice.type = 'basic';
						if(selectedOffices.length <= 5)
						{
							selectedOffices.push(selectedOffice);
						}

						//console.log(JSON.stringify(selectedOffice));
					}else if(abcMap[key].offices[key2].isSelected == false && selectedIds.includes(abcMap[key].offices[key2].office.Id))
					{
						for(var i = 0;i < selectedOffices.length;i++)
						{
							if(selectedOffices[i].Name == abcMap[key].offices[key2].office.Name)
							{
								selectedOffices.splice(i, 1);
							}
						}
					}
				}
			}
			cmp.set("v.chosenSubs", selectedOffices);
			
			if(selectedOffices.length > 5)
			{
				console.log("in else");
				// for(const key in abcMap)
				// {
				// 	for(const key2 in abcMap[key].offices)
				// 	{
				// 		if(abcMap[key].offices[key2].office.Id == clickedId)
				// 		{
				// 			abcMap[key].offices[key2].isSelected = false;
				// 		}
				// 	}
				// }
				// cmp.set("v.abcMap", abcMap);
				for(const key in abcMap1)
				{
					for(const key2 in abcMap1[key].offices)
					{
						if(abcMap1[key].offices[key2].office.Id == clickedId)
						{
							abcMap1[key].offices[key2].isSelected = false;
						}
					}
				}
				cmp.set("v.abcMap1", abcMap1);
				for(const key in abcMap2)
				{
					for(const key2 in abcMap2[key].offices)
					{
						if(abcMap2[key].offices[key2].office.Id == clickedId)
						{
							abcMap2[key].offices[key2].isSelected = false;
						}
					}
				}
				cmp.set("v.abcMap2", abcMap2);
				for(const key in abcMap3)
				{
					for(const key2 in abcMap3[key].offices)
					{
						if(abcMap3[key].offices[key2].office.Id == clickedId)
						{
							abcMap3[key].offices[key2].isSelected = false;
						}
					}
				}
				cmp.set("v.abcMap3", abcMap3);
			}
			//console.log(JSON.stringify(selectedOffices));
			// cmp.set("v.chosenSubs", selectedOffices);
		}catch(err)
		{
			console.log(err.message);
		}
	},
})