({
	doInit : function(cmp, evt, helper) 
	{
		try 
		{
			var str = location.search.substring(1);
			var searchObj = (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
			for(const key in searchObj)
			{
				searchObj[key] = decodeURIComponent(searchObj[key]);
			}
			searchObj.ErrorDescription = searchObj.ErrorDescription.replace(/\+/g, " ");
			cmp.set("v.errorDesc", searchObj.ErrorDescription);
		} catch (err) 
		{
			console.log("err:" + err.message);
		}
	}
})