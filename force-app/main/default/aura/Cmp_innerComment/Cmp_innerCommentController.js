({
    showText: function(component, event, helper) {
        if (event.target.classList.contains('slds-truncate')) {
            event.target.classList.remove('slds-truncate');
            event.target.classList.add('preStyle');
            event.getSource().set("v.label", $A.get("$Label.c.Hide"));

        } else {
            event.target.classList.add('slds-truncate');
            event.target.classList.remove('preStyle');
            event.getSource().set("v.label", $A.get("$Label.c.More"));

        }
    },
    doInit: function(cmp, evt, helper) {
        var cmt = cmp.get("v.comment");
        cmp.set("v.orgBody", cmt.mBody);
        cmp.set("v.publishChkbx", cmt.mIsDisp);
        console.log('cmt = ' + JSON.parse(JSON.stringify(cmt)) );
        console.log(JSON.stringify(cmt.mCommentReplys));
    },
    toggleEdit: function(cmp, evt, helper) {
        var isEdit = cmp.get("v.edit");
        cmp.set("v.edit", !isEdit);
    },
    save: function(cmp, evt, helper) {
        var cmt = cmp.get("v.comment");
        var action = cmp.get("c.updateComment");
        action.setParams({ cmtId: cmp.get("v.comment").mId, body: cmp.get("v.comment").mBody, show: cmp.get("v.publishChkbx") });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
               cmp.set("v.orgBody", cmt.mBody);
               cmp.set("v.edit", false);
            }
        });
        $A.enqueueAction(action);
    },
    cancel: function(cmp, evt, helper) {
        var cmt = cmp.get("v.comment");
        cmt.mBody = cmp.get("v.orgBody");
        cmp.set("v.comment", cmt);
        cmp.set("v.edit", false);
    },
    Go2CommentPage: function(cmp, event, helper) {
        var item = cmp.get("v.comment");
        window.location.href = "/s/law-item/" + cmp.get("v.comment.mLawItemId") + '&comment=' + item.mId;// + '&draftid=' +  cmp.get("v.comment.mId");
    }
})