({
    markAsRead: function (cmp, event, helper) 
    {
        var comment = cmp.get("v.comment");
        
        if ( comment.IsUnread!==false ) {
            var registerUserEvent = $A.get("e.c:Evt_commentMarkAsRead"); 
            registerUserEvent.setParams({ "commentId":comment.Id });
            registerUserEvent.fire();	
        }
	}
})