({
	onUserData: function(component, event, helper) {
		try {
			var currentUser = event.getParam("currentUser");
	
			if ( currentUser.Name ) {
				component.set("v.currentUser", currentUser );	
				component.set("v.isConnected", true);
			}
			else {
				component.set("v.isConnected", false);
			}
		} catch (error) {
			alert(error.message);
		}
	},

	toggleMainText: function(cmp, evt, helper) {
		cmp.set("v.showShortTextOnMobile", ! cmp.get("v.showShortTextOnMobile"));
		
	},

	doInit : function(component, event, helper) {
		var ParamsStr = window.location.search.substr(1);
		var urlParams = {};

		ParamsStr.split("&").forEach(function(element) {
			var keyValPair = element.split("=");
			urlParams[keyValPair[0]] = decodeURIComponent(keyValPair[1]);
		});

		var recordId; 
		if ( window.location.pathname.match("/law-item\/([^\/]*)") ) {
			recordId = window.location.pathname.match("/law-item\/([^\/]*)")[1];
		}
		else {
			recordId = urlParams.tzkir;
		}

		var commentId = null; 
		if(urlParams.draftid){
			commentId = urlParams.draftid; 
		}
		if(urlParams.comment)
		{
			var cmt = component.find(urlParams.comment);
			cmt = document.getElementById(urlParams.comment);

			if(cmt)
			{
				cmt.scrollToView();
			}
		}
		component.set("v.recordId", recordId);
		component.set("v.myUrl", window.location.href);

	
		var radioValues = [{'label': $A.get("$Label.c.Government_offices_only_radio_Button"), 'value': 'private'},{'label': $A.get("$Label.c.All_user_website_radio_Button"), 'value': 'public'}]
		component.set("v.radioOptions", radioValues);
		var action = component.get("c.getLawItem");
		
		action.setParams({"lawItemId": recordId, "draftId": commentId});
		action.setCallback(this, function (response) {
			var state = response.getState();

			if(component.isValid() && state === "SUCCESS"){
				var returnValue = response.getReturnValue();

				returnValue.bodyParagraphs = [];
				if( returnValue.DocumentBrief ) {
					returnValue.bodyParagraphs =  returnValue.DocumentBrief.split("\n");
				}

				component.set("v.lawItem", returnValue);

				if(returnValue.Draft){
					component.set("v.commentId", returnValue.Draft.Id);
					component.set("v.commentBody", returnValue.Draft.Body);
					component.set("v.lawItem.mNewUploadedFiles", returnValue.Draft.Files);
					component.set("v.radioValue", returnValue.Draft.IsPrivate ? 'private' : 'public');

					returnValue.Draft.commentSections.forEach(function(commentSection){
						helper.addSectionComment(component , commentSection);
					});
				
				}
			} 
		});
		$A.enqueueAction(action);
	},

	createSubComponent : function(cmp, event, helper){
		helper.addSectionComment(cmp);
	},
	
	showFullSideBar: function(component, event, helper){
		component.set('v.sidebarShowFull', !component.get('v.sidebarShowFull'));
	},

	saveComment : function(component, event, helper){
		var displayCommentError = false;
		var displayRadioError = false; 
		var subSectionsError = false; 
		

		
		var radioValue = component.get("v.radioValue");
		if (! radioValue ) {
			displayRadioError = true;
			component.find("visibility").focus();
			
		}

		var subSections = component.get("v.subComments");
		var hasSubSections = false;

		for ( var subSectionIndex = subSections.length -1 ; subSectionIndex >= 0; subSectionIndex-- ) {
			var subSection =  subSections[subSectionIndex];

			hasSubSections =  true;
			if ( !subSection.Comment ) {
				subSectionsError = true;
				helper.findElement(component, "sectionMessage_"+subSectionIndex).focus();
				subSections[subSectionIndex].Mode = 'sectionMessageError';
			}
			if ( !subSection.Section  ) {
				subSectionsError = true;
				helper.findElement(component, "section_"+subSectionIndex).focus();
				subSections[subSectionIndex].Mode = 'sectionError';
			}
		}

		component.set("v.subComments" , subSections);

		var fullText = component.get("v.commentBody");
		if ( !fullText && !hasSubSections ) {
			displayCommentError = true;
			component.find("addCommentText").focus();
		}

		
		component.set('v.displayCommentError', displayCommentError);
		component.set('v.displayRadioError', displayRadioError);

		if( displayRadioError || displayCommentError || subSectionsError ) {
			return;
		}
		
		var status = event.currentTarget.dataset.status;

		var isPrivateComment = radioValue;
		isPrivateComment = isPrivateComment == 'private'?true:false;

		var action = component.get('c.updateComment');
		action.setParams({  
							lawItemId: component.get('v.recordId'),
							commentId: component.get('v.commentId'),
							commentBody: fullText,
							commentSections: subSections,
							isPrivate: isPrivateComment,
							commentStatus: status
						});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if(component.isValid() && state === 'SUCCESS'){
				var newComment = response.getReturnValue();
				var lawItem = component.get("v.lawItem");
				lawItem.Comments.push(newComment);
				component.set("v.lawItem", lawItem);
			} 
			window.location.reload();
		});
		$A.enqueueAction(action);
	},

	goToPreviousPage : function(component, event, helper){
		window.history.go(-1);
	},

	goToLoginPage : function(component, event, helper){
		window.location.assign("/s/moj-login");
	},

	openModal : function(component, event, helper){
		try {
			var commentSection = component.find(event.currentTarget.dataset.type);
			console.log("commentSection", commentSection);
			
			$A.util.addClass(commentSection, 'disabled-div');
			// event.currentTarget.classList.add("disabled-div");
			component.set("v.showConectBubble", true);
		} catch (error) {
			alert(error.message);
		}
	},

	closeModal : function(component, event, helper){
		try {
			var commentSection = component.find(event.currentTarget.dataset.type);
			component.set("v.showConectBubble", false);
			$A.util.removeClass(commentSection, 'disabled-div');
		} catch (error) {
			alert(error.message);
		}
	},

	shareWithFacebook : function(component, event, helper){
		var path = 'https://www.facebook.com/sharer/sharer.php?u=' +  window.location.href.replace(/\&/g, "%26").replace(/\=/g, "%3D");
		window.open(path, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=700, height=500');
	},

	shareWithWhatsapp : function(component, event, helper){
		var path = 'https://api.whatsapp.com/send?text=' +  window.location.href.replace(/\&/g, "%26").replace(/\=/g, "%3D");
		window.open(path, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=700, height=500');
	},

	shareWithGmail : function(component, event, helper){
		var path = 'mailto:?body=' + location.origin + '/s/tzkirim?tzkir=' + component.get("v.lawItem").mId;
		location.href = path;
	},
	
	removeSubComment: function(cmp, event, helper) {
		var subCommentsIndex = event.target.getAttribute("data-itemIndex");
		
		var subComments = cmp.get('v.subComments');
		subComments.splice(subCommentsIndex, 1);
		cmp.set('v.subComments', subComments);
		
		var wrapper = helper.findElement(cmp, "sectionWrapper_"+subComments.length);
		var domElementToRemove = wrapper.getElements()[0];
		domElementToRemove.parentNode.removeChild(domElementToRemove);
	},

	showShare: function(component, event, helper) {
		document.getElementById('sharemenu').classList.toggle("HideMenu");
	},

	showText: function (component, event, helper) {
		try {
			var lawItemPage = component.find(event.currentTarget.dataset.id);
			// $A.util.removeClass(lawItemPage, 'text-truncate');
			$A.util.removeClass(lawItemPage, event.currentTarget.dataset.class);

			var showMore = event.currentTarget;
			$A.util.addClass(showMore, 'slds-hide');
			
			var showLess = document.getElementById(event.currentTarget.dataset.opposite);
			$A.util.removeClass(showLess, 'slds-hide');
		} catch (error) {
			alert("ERROR: " + error.message);
		}
	},
    hideText: function (component, event, helper) {
		try {
			var lawItemPage = component.find(event.currentTarget.dataset.id);
			$A.util.addClass(lawItemPage, event.currentTarget.dataset.class);
			
			var showLess = event.currentTarget;
			$A.util.addClass(showLess, 'slds-hide');
			
			var showMore = document.getElementById(event.currentTarget.dataset.opposite);
			$A.util.removeClass(showMore, 'slds-hide');
			
		} catch (error) {
			alert("ERROR: " + error.message);
		}
	},
})