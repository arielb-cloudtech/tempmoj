({
	checkWindowSize: function (component) {
		if (window.innerWidth < 992) {
			component.set("v.isLargeSizeScreen", false);
		} else {
			component.set("v.isLargeSizeScreen", true);
		}
	},
	findElement: function (cmp, elementId) {
		var elements = cmp.find(elementId);
		
		if ( elements.length ){
			return elements[ elements.length - 1 ];
		}
		else {
			return elements;
		}
	},
	addSectionComment: function(cmp, commentSection){
		
		if ( !commentSection ) {
			commentSection = {Id:'', Mode: 'new' , Section: '', Comment:'' };
		}
		
		
		var subComments = cmp.get('v.subComments');
		var subCommentIndex = subComments.length;
		subComments.push(commentSection);
		cmp.set('v.subComments', subComments);
		
		$A.createComponents(
			[
				[	
					"aura:html",
					{ 
						"aura:id" : "sectionWrapper_"+subCommentIndex,
						tag: "div",
						HTMLAttributes:{
							"class": cmp.getReference("v.subComments["+subCommentIndex+"].Mode"),
						}
					}
				],
				[	
					"aura:html",
					{ 
						tag: "div",
						HTMLAttributes:{
							"class": "commentSectionFields",
						}
					}
				],
				[   
					"lightning:input",
					{
						"aura:id" : "section_"+subCommentIndex,
						"value": cmp.getReference("v.subComments["+subCommentIndex+"].Section"),
						"class":"sectionLabel", 
						ariaLabel:'מספר סעיף'
						
					}
				],
				[   
					"lightning:textarea",
					{
						"aura:id" : "sectionMessage_"+subCommentIndex,
						"value": cmp.getReference("v.subComments["+subCommentIndex+"].Comment"),
						"class":"sectionText", 
						"label":"הערות לסעיף"
					}
				],
				[
					"aura:html",
					{ 
						tag: "button",
						HTMLAttributes:{
							"class": "trash", 
							"data-itemIndex": subCommentIndex,
							"onclick": cmp.getReference("c.removeSubComment"),
							"aria-label": "מחיקה"
						}
					}
				],
				[
					"aura:html",
					{ 
						tag: "div",
						body:   $A.get("$Label.c.Error_No_Comment_Body"),
						HTMLAttributes:{
							"class": "errorMessage"
						}
					}
				],
			],
			function(component, status, errorMessage){
				if ( status === "SUCCESS") {
					
					var fieldWrapper = component[1];
					var fieldWrapperbody = fieldWrapper.get("v.body");
					fieldWrapperbody.push(component[2]);   
					fieldWrapperbody.push(component[3]);   
					fieldWrapperbody.push(component[4]);
					fieldWrapper.set("v.body", fieldWrapperbody);

					var wrapper = component[0];
					var wrapperbody = wrapper.get("v.body");
					wrapperbody.push(fieldWrapper);   
					wrapperbody.push(component[5]);   
					wrapper.set("v.body", wrapperbody);
					
					var commentSections = cmp.find('commentSections');
					var body = commentSections.get("v.body");
					body.push(wrapper);
					commentSections.set("v.body", body);
					
					setTimeout(function(){
						
					component[2].focus();
					// document.getElementById(component[3].getLocalId("sectionMessage_"+subCommentIndex));
				    // component.find("sectionMessage_"+subCommentIndex).HTMLAttributes("aria-labelledby: vgvgvgvgvgvg");
						//component[3].setAttribute("aria-label",'aaaaaaa');
				},10);
					
				}
			});
	}
})