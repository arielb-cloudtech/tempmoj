({ 
	removeFile : function(component, event, helper) {
		component.set("v.deleteFile", false);
		console.log('removeFile = ' + component.get("v.file.documentId"));
		 var eve = $A.get("e.c:LawItem_DeleteCreatedFile");
		 eve.setParams({ "docId": component.get("v.file.documentId") });
		 eve.fire();
	}
})